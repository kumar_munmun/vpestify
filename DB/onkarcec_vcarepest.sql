-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 26, 2019 at 08:19 AM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onkarcec_vcarepest`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_imagetype`
--

CREATE TABLE `tbl_imagetype` (
  `Image_Type_Id` int(11) NOT NULL,
  `Image_Type_Name` varchar(1000) NOT NULL,
  `ImageTypeContent` longtext NOT NULL,
  `IsActive` int(11) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` varchar(100) NOT NULL,
  `ModifiedBy` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_imagetype`
--

INSERT INTO `tbl_imagetype` (`Image_Type_Id`, `Image_Type_Name`, `ImageTypeContent`, `IsActive`, `CreatedDate`, `ModifiedDate`, `CreatedBy`, `ModifiedBy`) VALUES
(56, 'Home', 'Home Content', 1, '0000-00-00 00:00:00', '2017-08-29 07:07:20', '', ''),
(57, 'About', 'vPestify Service is growth company in pest control industries. We are specialize in reactive as well as proactive and preventative pest control and with our swift response and industries leading treatments we can Keep Your Home or business Pest free. We have a range of unique, innovative, non-toxic solutions to control pest activity and provide on-going protection. We provide a swift response, with fast detection and effective pest control solutions, through our team of local experts. All our service technicians go through our rigorous training program to ensure we have the highest qualified pest control employees in Delhi.', 1, '0000-00-00 00:00:00', '2017-08-29 07:07:41', '', ''),
(58, 'Contact', 'Contact Content', 1, '0000-00-00 00:00:00', '2017-08-29 07:08:14', '', ''),
(59, 'Services', 'Services Content edit', 1, '0000-00-00 00:00:00', '2017-08-29 08:13:40', '', ''),
(60, 'Safety', 'Safety Content', 1, '0000-00-00 00:00:00', '2017-08-30 07:58:43', '', ''),
(61, 'ABC', 'ABc Content', 1, '0000-00-00 00:00:00', '2018-12-02 15:03:41', '', ''),
(62, 'Termite', 'termite', 1, '0000-00-00 00:00:00', '2019-01-12 09:33:07', '', ''),
(63, 'Rats', 'Rats', 1, '0000-00-00 00:00:00', '2019-01-12 09:33:27', '', ''),
(64, 'Cockroach', 'Cockroach', 1, '0000-00-00 00:00:00', '2019-01-12 09:33:49', '', ''),
(65, 'Bedbug Control', 'Bedbug Control', 1, '0000-00-00 00:00:00', '2019-01-15 14:39:53', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_imageupload`
--

CREATE TABLE `tbl_imageupload` (
  `Image_Id` int(11) NOT NULL,
  `Image_Type_Id` int(11) NOT NULL,
  `Image_Title` text NOT NULL,
  `Upload_Image` text NOT NULL,
  `Image_Place` varchar(100) NOT NULL,
  `Image_Content` longtext NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` varchar(100) NOT NULL,
  `ModifiedBy` varchar(100) NOT NULL,
  `IsActive` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_imageupload`
--

INSERT INTO `tbl_imageupload` (`Image_Id`, `Image_Type_Id`, `Image_Title`, `Upload_Image`, `Image_Place`, `Image_Content`, `CreatedDate`, `ModifiedDate`, `CreatedBy`, `ModifiedBy`, `IsActive`) VALUES
(4, 56, 'Slider Image', 'ImageUpload/1548323270Slider_Image.png', 'slider', 'Slide Image', '2019-01-24 15:17:49', '2017-08-29 07:13:36', '', '', b'1'),
(5, 56, 'Slider Image', 'ImageUpload/1548326038Slider_Image.png', 'slider', 'Slider Image', '2019-01-26 12:34:50', '2017-08-29 07:14:14', '', '', b'1'),
(6, 57, 'About Image', 'ImageUpload/1503991456About_Image.jpg', 'top', 'About Content&nbsp;', '2017-08-29 12:54:16', '2017-08-29 07:24:16', '', '', b'1'),
(7, 56, 'Top Image', 'ImageUpload/1503993149Top_Image.jpg', 'middel', 'Middle Home Image', '2017-08-29 13:22:29', '2017-08-29 07:52:30', '', '', b'1'),
(8, 0, '', 'ImageUpload/1504001216.jpg', '', '', '2017-08-29 15:36:56', '2017-08-29 10:06:56', '', '', b'1'),
(9, 57, 'sdsd', 'ImageUpload/1507375422sdsd.jpg', 'bottom', 'dcsdcfsd', '2017-10-07 16:53:42', '2017-10-07 11:23:42', '', '', b'1'),
(10, 61, 'ABC Ttile', 'ImageUpload/1543763088ABC_Ttile.jpg', 'slider', 'jbjhdgjhds bifbdkf', '2018-12-02 20:34:48', '2018-12-02 15:04:48', '', '', b'0'),
(11, 56, 'ABC Ttile gfg', 'ImageUpload/1545111025ABC_Ttile_gfg.jpg', 'right', 'gfh hgjhhgjyvjhtyfyf', '2018-12-18 11:00:25', '2018-12-18 05:30:25', '', '', b'0'),
(12, 62, 'Termite', 'ImageUpload/1547293486Termite.png', 'top', 'asdfdsf', '2019-01-12 17:14:46', '2019-01-12 11:44:46', '', '', b'1'),
(13, 56, 'Termite', 'ImageUpload/1548323565Termite.png', 'slider', 'Termite', '2019-01-24 15:22:45', '2019-01-15 14:36:47', '', '', b'1'),
(14, 65, 'Bedbug Control', 'ImageUpload/1547563210Bedbug_Control.jpg', 'top', 'Bedbug Control', '2019-01-15 20:10:10', '2019-01-15 14:40:10', '', '', b'1'),
(15, 56, 'Termite', 'ImageUpload/1548324263Termite.png', 'slider', '', '2019-01-24 15:34:23', '2019-01-24 10:04:23', '', '', b'1'),
(16, 58, 'Contact', 'ImageUpload/1548325138Contact.png', 'top', 'Contact Us', '2019-01-24 15:48:58', '2019-01-24 10:18:58', '', '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pestservices`
--

CREATE TABLE `tbl_pestservices` (
  `Services_Id` int(11) NOT NULL,
  `Service_Name` varchar(100) NOT NULL,
  `Service_Content` longtext NOT NULL,
  `Service_Image` text NOT NULL,
  `IsActive` bit(1) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreatedBy` varchar(100) NOT NULL,
  `ModifiedBy` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pestservices`
--

INSERT INTO `tbl_pestservices` (`Services_Id`, `Service_Name`, `Service_Content`, `Service_Image`, `IsActive`, `CreatedDate`, `ModifiedDate`, `CreatedBy`, `ModifiedBy`) VALUES
(1, 'termite', '<p><strong>Termites</strong>&nbsp;are known as &ldquo;silent destroyer&rdquo; because they may be secretly hiding in your home. Without any immediate sings of damage they can be start damaging your home or property.</p>\r\n<p>All types of termite consume cellulose. Unfortunately all cellulose based-plant like all homes can provide cellulose for termite infestation.&nbsp;<strong>vPestify&nbsp;</strong>has developed an integrated termite control service to suit your needs.</p>\r\n<div><span lang=\"EN-IN\"><strong>1.Formosan termites</strong></span></div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-IN\">Formosan termites are social worker insects.</span></div>\r\n<div><span lang=\"EN-IN\">The Formosan termites are known as the subterranean species of termites. And referred as &ldquo;super termites&rdquo; due to aggressive in nature.</span></div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-IN\">&nbsp; &nbsp; &nbsp;They have three castes</span></div>\r\n<div><span lang=\"EN-IN\">&nbsp; &nbsp; &nbsp;1. Workers</span></div>\r\n<div><span lang=\"EN-IN\">&nbsp; &nbsp; &nbsp;2. Soldiers</span></div>\r\n<div>&nbsp; &nbsp; &nbsp;3. Alates(reproductive).</div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-IN\">The alates or Flying termites (Winged reproductive) are associated with the reproduction process of the species. Flying termites can fight with Ants. They have the sole purpose to start a new colony and want to become king and queen of a colony.</span></div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-IN\"><strong>Identification:</strong></span></div>\r\n<div>&nbsp;</div>\r\n<div><span lang=\"EN-IN\">Colour- creamy white to brown</span></div>\r\n<div><span lang=\"EN-IN\">They have 6 legs, Shape &ndash; long narrow and oval and Size &ndash; &frac12; inch in length.</span></div>\r\n<div>&nbsp;</div>\r\n<div><strong>&nbsp; &nbsp; Types Of Termite Species :</strong></div>\r\n<div>&nbsp;</div>\r\n<div><strong>&nbsp; &nbsp; (1) Dampwood termites</strong></div>\r\n<p><strong>&nbsp;</strong>Dampwood termites infest wood-based product with high moisture content. These&nbsp; &nbsp;termites are normally larger in size than other species of termite. dampwood&nbsp; &nbsp;termites do not construct mud tubes but have the ability to drill holes in the&nbsp; &nbsp; &nbsp; &nbsp;wooden structure.</p>\r\n<p>&nbsp;Dampwood termites are often found in Pacific coastal and adjacent states, the&nbsp; &nbsp; &nbsp; &nbsp;desert or semi-arid southwest.</p>\r\n<p>&nbsp;Dampwood termites weaken homes by hollowing out support beams.</p>\r\n<p>&nbsp; &nbsp; &nbsp;<strong>Identification :&nbsp;</strong></p>\r\n<p>&nbsp; &nbsp; &nbsp;Several species of damp wood termites:&nbsp;</p>\r\n<p><strong>&nbsp; &nbsp; &nbsp;(A) Swarmers</strong>: The Swarmers (winged termites) have 25 mm long, with their wings.</p>\r\n<p><strong>&nbsp; &nbsp; &nbsp;(B) Soldiers</strong>: as large as 20 mm.</p>\r\n<p><strong>&nbsp; &nbsp; &nbsp;(C) Immature</strong>: as much as 20 mm long. These termites do the work in the colony.</p>\r\n<p>&nbsp; &nbsp; &nbsp;They have a large head with mandibles on the front.</p>\r\n<p><strong>(2) Drywood termites</strong></p>\r\n<p>Drywood termites infest on those varieties of wood that are devoid of moisture. These termites do not need to come in contact with the soil unlike the subterranean and Formosan termites These species of termite establish their nests on wooden materials and wooden wall supports, they don&rsquo;t need as much as moisture for survival as other species.</p>\r\n<p>They can be found near leaky pipes and drains, thereby are most dangerous.</p>\r\n<p>Drywood termites infest dry wood and can be infested dead wood but do not require contact with the soil,</p>\r\n<p><strong>Identification:</strong></p>\r\n<p>Colour -light brown to creamy white, grow up to 1 inch, have six legs and Shape of these species is narrow, long and oval.</p>\r\n<p><strong>(3) Conehead termites</strong></p>\r\n<p>Conehead termites come from the cone-shaped heads from soldiers.</p>\r\n<p>Conehead termites originally called &ldquo;tree termites&rdquo; because so many people mistakenly believed that these termites only found in a tree but they are also attacking homes and many other items made of wood</p>\r\n<p>These termites are an invasive species found in the southern part of the state.</p>\r\n<p>coneheads build wider and more extensive mud tunnels above the ground not in the ground.</p>\r\n<p>Colour Cream bodies, Dark brown heads.</p>\r\n<p>Legs the&nbsp;</p>\r\n<p><strong>Identification:</strong></p>\r\n<p>Colour - Cream bodies, Dark brown heads.</p>\r\n<p>Legs -&nbsp;6</p>\r\n<p>The shape is long, narrow, soldiers have a pear-shaped head</p>\r\n<p>Size 3 - 4mm.</p>\r\n<p>There are an estimated 4,000 species (about 2,600 taxonomically known). About 10% are pests&nbsp;which can cause serious structural damage to building, crops or plantation forests.</p>\r\n<p>&nbsp;<strong>Termites</strong>&nbsp;are major detrivers&nbsp;in the subtropical&nbsp;and tropical&nbsp;regions. Their recycling of wood and other plant matter is very important for ecology.</p>\r\n<p><strong>Termites</strong>&nbsp;live in colonies that, at maturity, hold from several hundred to several million individuals. They are a prime example self-organised systems which use swarm intelligence. They use this cooperation to exploit food sources and environments which would not be available to any single insect acting alone.</p>\r\n<p>&nbsp;<strong>Eliminate Moisture Problems</strong></p>\r\n<ul>\r\n<li>We should always repair leaking faucets, water pipes, and A/C units, cooler</li>\r\n<li>To divert water from foundation</li>\r\n<li>Always Keep gutters and downspouts clean</li>\r\n<li>We shoud remove excessive plant cover and wood mulch</li>\r\n<li>Getting rid of standing water on roof and floor</li>\r\n<li>To keep all vents clear and open</li>\r\n<li>To seal entry points around water and utility lines or pipes.</li>\r\n</ul>', 'ImageService/1546350852Termite.png', b'1', '2019-01-15 22:03:13', '2019-01-15 16:33:13', '', ''),
(2, 'rats', '<p><strong>Rats</strong>&nbsp;are mammals which are characterized by a single pair of growing incisors in each of the upper and lower jaws</p>\r\n<p>About 40% of 2,277 mammal species are rodents.</p>\r\n<p>Most rodents are small animals with short limbs, robust bodies, and long tails. They use their sharp incisors to gnaw food, and defend themselves. Most rodents eat seeds or other plant material and also some rodents are herbivorous. Most rodents tend to be social animals and many species live in societies.</p>\r\n<p><span lang=\"EN-IN\">There are many different types of rodents can be distinguished by their differences in physical appearance as well as genetics.</span></p>\r\n<div><span lang=\"EN-IN\">Chipmunks, marmots, woodchucks, squirrels, prairie dogs and gophers belong to one rodent group. Common house mice, rats, gerbils, hamsters, lemmings, and voles are another group.</span></div>\r\n<div>&nbsp;</div>\r\n<div>But generally, Rodents are characterized into two types.</div>\r\n<div>&nbsp;</div>\r\n<div><strong>(1) MOLE RODENT&nbsp;</strong><strong>(2) DESEART RODENT</strong></div>\r\n<div>&nbsp;</div>\r\n<div><strong>(1) MOLE RODENT</strong></div>\r\n<div>The common mole rodents are the blind mole rat, naked mole rat. These rodents are best known for their propensity to burrow. Mole rodents are typically small and grey in colour. Their size ranges from 5 to 10 cm in length, Mole rodents have lack ears.</div>\r\n<div>&nbsp;</div>\r\n<div><strong>(2)&nbsp;DESEART&nbsp;</strong>&nbsp;<strong>RODENT</strong></div>\r\n<div>\r\n<p>Jerboa, hopping mice and kangaroo rats adapt to desert environments. They have highly developed hind legs, live in deep burrows and rarely drink water. Desert rodents consume moisture from food and conserve water through a metabolic process.</p>\r\n<p>Hopping mice are known as Australian kangaroo rats. These rodents are typically brown or fawn in colour. Hopping mice have extremely long tails.</p>\r\n<p>They feed on seeds, stems, buds, fruits and insects. these rats have large cheek pockets for food storage.</p>\r\n<p><strong>RATS:</strong></p>\r\n<p><strong>(1) Cotton Rats</strong></p>\r\n<p>The head and body of these pests range in length from 13.3 to 21.3&nbsp;&nbsp; cm with 7.6 to 16.5 cm tails in length.</p>\r\n<p>Cotton rats are usually grey on their back with black hairs. Their undersides are light in colour.</p>\r\nTheir bodies are covered with coarse hair. Cotton rats are agricultural pests that may damage to row crops. The area where cotton rats prefer to live is grassy and feed on plants in crop fields.</div>\r\n<div>\r\n<p><strong>(2) DEER MICE</strong></p>\r\n<p>&nbsp;&nbsp;They are round and slender, body length from 7 to 10 cm long.</p>\r\n<ul>\r\n<li>Deer mice have a pointed nose and large, black eyes. Their ears are large with a little fur.</li>\r\n<li>Their bodies are bicolored with a light brownish to the reddish top and hite underbelly and feet.</li>\r\n<li>These pests have short tails, distinctly bicolored, and covered with short, fine hairs and have 5 to 13 cm in length.</li>\r\n</ul>\r\n</div>', 'ImageService/1546350877Rats.png', b'1', '2019-01-15 22:29:05', '2019-01-15 16:59:05', '', ''),
(3, 'cockroach', ' Cockroach Cockroach hudgj ih f ds sdiic jh id 2017 October 33', 'ImageService/1546350894Cockroach.png', b'1', '2019-01-09 17:58:21', '2019-01-09 12:28:21', '', ''),
(4, 'bedbug control', '<p>These insects typically live in temperate climates. <strong>Bed</strong> <strong>bugs</strong> are troublesome household pests found in the world.</p>\r\n<p>Bed bugs feed on the blood of humans, it is possible to transmit disease, and in India, it is very rare. Bed bug bites do not hurt but it is the itchiness that causes discomfort.</p>\r\n<p>Adult Bed bugs are 5mm long with a flattened oval shape and light brown color. After feeding it become rounder and darker.</p>\r\n<p>&nbsp;They are attracted to the warmth of our bodies. During the daytime, they are usually found in bedrooms and hide in cracks and crevices</p>\r\n<p>Bed bugs usually carried on clothing or inside furniture.&nbsp; Hotels, cinemas, lodges, auditoriums, and overnight buses are the most common places for bed bug infestations.</p>\r\n<p>&nbsp;You can keep Bed bug numbers under control by the use of High levels of hygiene, deep cleaning and the use of amateur insecticides.</p>\r\n<p><strong>vPestify&nbsp;</strong>has developed an integrated bed bug control service to suit your needs.</p>', 'ImageService/1546351067Bedbug_Control.png', b'1', '2019-01-15 22:12:56', '2019-01-15 16:42:56', '', ''),
(5, 'ABC', 'kjbcjhdsgcjhdc', 'ImageService/1543762854ABC.jpg', b'0', '2018-12-02 20:30:54', '2018-12-18 05:31:14', '', ''),
(6, 'rock dove pigeons', 'jhfhgfhgh', 'ImageService/1546352351Flies.png', b'1', '2019-01-09 17:58:44', '2019-01-09 12:28:44', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_userenquirydetails`
--

CREATE TABLE `tbl_userenquirydetails` (
  `Enquiry_Id` int(11) NOT NULL,
  `User_Name` varchar(100) NOT NULL,
  `User_Email` varchar(200) NOT NULL,
  `Contact_No` bigint(20) NOT NULL,
  `Enquiry_Details` text NOT NULL,
  `Enquiry_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_userenquirydetails`
--

INSERT INTO `tbl_userenquirydetails` (`Enquiry_Id`, `User_Name`, `User_Email`, `Contact_No`, `Enquiry_Details`, `Enquiry_Date`, `IsActive`) VALUES
(1, 'user', 'email', 908798, 'about', '2017-08-30 15:04:16', b'1'),
(2, 'ee', 'ee', 656, 'sdfds', '2017-08-30 15:07:38', b'1'),
(3, 'test', 'test@gmail.com', 7576576576, 'fhgfhfhh ytfu', '2017-12-06 18:07:40', b'1'),
(4, 'test1', 'test@gmail.com', 7777777777, 'mesg', '2017-12-25 17:13:43', b'1'),
(5, 'Viv', 'vivek2umar93@gmail.com', 9650539282, 'Dhu', '2019-01-10 10:49:49', b'1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_imagetype`
--
ALTER TABLE `tbl_imagetype`
  ADD PRIMARY KEY (`Image_Type_Id`);

--
-- Indexes for table `tbl_imageupload`
--
ALTER TABLE `tbl_imageupload`
  ADD PRIMARY KEY (`Image_Id`);

--
-- Indexes for table `tbl_pestservices`
--
ALTER TABLE `tbl_pestservices`
  ADD PRIMARY KEY (`Services_Id`);

--
-- Indexes for table `tbl_userenquirydetails`
--
ALTER TABLE `tbl_userenquirydetails`
  ADD PRIMARY KEY (`Enquiry_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_imagetype`
--
ALTER TABLE `tbl_imagetype`
  MODIFY `Image_Type_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `tbl_imageupload`
--
ALTER TABLE `tbl_imageupload`
  MODIFY `Image_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_pestservices`
--
ALTER TABLE `tbl_pestservices`
  MODIFY `Services_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_userenquirydetails`
--
ALTER TABLE `tbl_userenquirydetails`
  MODIFY `Enquiry_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
