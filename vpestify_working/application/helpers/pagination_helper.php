<?php

/**
 * pagination helper( Help to configuration of paginations)
 * @Created By:  Nitish Kaushal
 * @Created On:  20 Aug 2015
 * @Email:       nitish.kaushal@apptology.com
 * 
 */

function get_pagination_list($redirect_url,$total_count,$per_page,$start){
    $ci = & get_instance();    
    //$redirect_url='manage_plan_category';
    
    // get get paramater
    $get_param=$ci->input->get();
    unset($get_param['start']);
    // if get parameter exist
    if(!empty($get_param)){
        // get param count
        $get_param_count=count($get_param);
        /* set new page limit */
        $per_page=isset($get_param['page_limit'])?$get_param['page_limit']:$per_page;
        // set redirect url
        $redirect_url.='?';
        $key_count=0;
        foreach($get_param as $key=>$val){
            if($key_count==0) $redirect_url.="$key=$val";                
            if($key!=$get_param_count && $key!=0)  $redirect_url.="&";
            $key_count++;
        }
    }    
    
    // Initialize empty array.
    $config = array();

    // Set base_url for every links
    $config["base_url"] = site_url($redirect_url);

    // Set total rows in the result set you are creating pagination for.
    $config["total_rows"] = $total_count;

    // Number of items you intend to show per page.
    $config["per_page"] = $per_page;
    
    // Number of page links
    //$config["uri_segment"] = 2;
    
    // Use pagination number for anchor URL.
    $config["use_page_numbers"] = TRUE;
    
    // Enable query string
    $config['enable_query_strings']=TRUE;
    $config['page_query_string']=TRUE;    
    $config['query_string_segment'] = 'start';
            
    // Full block
    $config["full_tag_open"] = '<ul class="pagination pagination-sm">';
    $config["full_tag_close"] = '</ul>';
    
    // By clicking on performing first pagination.
    $config["first_link"] = '<i class="fa fa-angle-double-left"></i>';
    $config["first_tag_open"] = '<li>';
    $config["first_tag_close"] = '</li>';
    
    // By clicking on performing last pagination.
    $config["last_link"] = '<i class="fa fa-angle-double-right"></i>';
    $config["last_tag_open"] = '<li>';
    $config["last_tag_close"] = '</li>';
    
    // By clicking on performing NEXT pagination.
    $config["next_link"] = '<i class="fa fa-angle-right"></i>';
    $config["next_tag_open"] = '<li>';
    $config["next_tag_close"] = '</li>';
    
    // By clicking on performing PREVIOUS pagination.
    $config["prev_link"] = '<i class="fa fa-angle-left"></i>';
    $config["prev_tag_open"] = '<li>';
    $config["prev_tag_close"] = '</li>';
    
    // By clicking on performing number pagination.
    $config["num_tag_open"] = '<li>';
    $config["num_tag_close"] = '</li>';
    
    // Open tag for CURRENT link.
    $config["cur_tag_open"] = '<li class="paginate_button active"><a href="#">';
    $config["cur_tag_close"] = '</a></li>';   

    //load pagination library
    $ci->load->library("pagination");
    
    // To initialize "$config" array and set to pagination library.
    $ci->pagination->initialize($config);

    // Create link.
    return $ci->pagination->create_links();
}