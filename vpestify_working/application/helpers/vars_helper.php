<?php

define('USR_SESSION_NAME', 'PicsUser');

define('URL', base_url(''));
define('AURL', base_url('assets/backend/') . '/');
define('IMURL', base_url('assets/backend/images') . '/');
define('REPORT_PDF', 'uploads/reports/');

define('ADM_URL', base_url('admin') . '/');
define('USR_URL', base_url('user') . '/');

define('REQ_URI', $_SERVER['REQUEST_URI']);

define('SITE_NAME', 'Food Safety');
define('VERSION', '1.01');

define('PAGE_SIZE', 30);
define('ADMIN_EMAIL', 'admin@audit.foodsafetyhelpline.com');
define('ADM_MOB', 8510074200);

define('AUDIT_IMAGE', 'assets/uploads/audit_images/');
define('AUDIT_SIGN', 'assets/uploads/audit_sign/');
define('SIGNATURES', 'assets/uploads/signatures/');
define('PROFILE_IMG_DIR', 'assets/uploads/users/profimg/');
define('MANUAL_PDF', 'assets/uploads/manual_pdf/');
define('SUBCAT_IMAGE', 'assets/uploads/subcat_image/');
?>
