<?php

function current_date() {
    return date('Y-m-d H:i:s');
}

function logged_user_data() {
    $data = get_session(USR_SESSION_NAME);
//    pr($data);
//    die();
    return $data;
}

function get_perms() {
    $data = get_session(USR_SESSION_NAME);
   
    return $data['perms'];
}

function is_logged() {
    if (defined('USER_ID') && USER_ID)
        return USER_ID;
}

function not_authorize() {
    $ci = &get_instance();
    $ci->load->view('layouts/not_auth');
}
function sendJson($payload=array(), $status=false, $msg='', $code='',$extraParameter = array()) {
	if(!$payload) $payload=new stdClass();
	
        echo json_encode(array('Payload'=>$payload, 'Status'=>$status, 'Message'=>$msg, 'Code'=>$code,));
	die;
}

/** Lookups * */
function status_lookup($status='', $arg=0) {
	if(!$arg)
		$a=array('1'=>'Active','0'=>'Inactive');
	else
		$a=array('1'=>'Active', '0'=>'Inactive');
	
	if($status!=='')
		return $a[$status];
	return $a;
}

/** Lookups End* */


/*answer type*/
function ans_type($status='', $arg=0) {
	if(!$arg)
		$a=array('1'=>'Yes','2'=>'No','3'=>'Partial');
	else
		$a=array('1'=>'Yes','2'=>'No','3'=>'Partial');
	
	if($status!=='')
		return $a[$status];
	return $a;
}
/*end of answer type*/


function filter_startAudit_back($results)
{
    $formated_result = array();
    if (!empty($results))
    {
        foreach ($results as $result)
        {
            if (!array_key_exists($result['catId'], $formated_result))
            {
                $formated_result[$result['catId']][$result['categoryName']]   = array();
            }
    
            $tmp_formated_result = array('quesId'=>$result['quesId'],
                'questionText'=>$result['questionText'],
                'numberOfSamples'=>$result['numberOfSamples'],
                'subCategoryName'=>$result['subCategoryName']
             );
            array_push($formated_result[$result['catId']][$result['categoryName']], $tmp_formated_result);
       }
    }
    return array_values($formated_result);
 }
 
function filter_startAudit($results)
{
    $formated_result = array();
    if (!empty($results))
    {
        foreach ($results as $result)
        {
            if (!array_key_exists($result['catId'], $formated_result))
            {
                $formated_result[$result['catId']]      = array();
                $formated_result[$result['catId']]['catId']   = $result['catId'];
                $formated_result[$result['catId']]['categoryName']  = $result['categoryName'];
                $formated_result[$result['catId']]['type']  = $result['type'];
                $formated_result[$result['catId']]['Questions']   = array();
                
            }
            $tmp_formated_result = array('quesId'=>$result['quesId'],
                'subCategoryId'=>$result['subCategoryId'],
                'questionText'=>$result['questionText'],
                'questionDesc'=>$result['questionDesc'],
                'numberOfSamples'=>$result['numberOfSamples'],
                'subCategoryName'=>$result['subCategoryName'],
             );
            array_push($formated_result[$result['catId']]['Questions'], $tmp_formated_result);
        }
  }
  return array_values($formated_result);
 }
 
 function filter_report_pdf($results)
{
    $formated_result = array();
    if (!empty($results))
    {
        foreach ($results as $result)
        {
			if($result['Status']=='yes')
				$report_url = base_url(REPORT_PDF.$result['pdf_one']);
			else if($result['Status']=='no')
				$report_url='';
            $tmp_formated_result = array('ReportId'=>$result['ReportId'],
                'ReportUrl'=>$report_url,
                'ReportDate'=>$result['ReportDate'],
                'Status'=>$result['Status'],
             );
            array_push($formated_result, $tmp_formated_result);
        }
  }
  return array_values($formated_result);
 }
 
function filter_answers($results)
    {
        $formated_result = array();
        if (!empty($results))
        {
            foreach ($results as $result)
            {  // pr($result);
                if (!array_key_exists($result['categoryId'], $formated_result))
                {
                    $formated_result[$result['categoryId']]      = array();
                    $formated_result[$result['categoryId']]['categoryId']   = $result['categoryId'];
                    $formated_result[$result['categoryId']]['categoryName']   = $result['categoryName'];
                    $formated_result[$result['categoryId']]['cat_skip']   = $result['cat_skip'];
                    $formated_result[$result['categoryId']]['status']   = $result['status'];
		    $formated_result[$result['categoryId']]['show_to_client']   = $result['show_to_client'];
                    $formated_result[$result['categoryId']]['get_report_in']   = $result['get_report_in'];
                    $formated_result[$result['categoryId']]['auditsId']   = $result['auditsId'];
                    $formated_result[$result['categoryId']]['startdateTime']   = $result['startdateTime'];
                    $formated_result[$result['categoryId']]['enddatetime']   = $result['enddatetime'];
                    $formated_result[$result['categoryId']]['previous_percentage']   = $result['previous_percentage'];
                    $formated_result[$result['categoryId']]['updatedBy']   = $result['updatedBy'];
                    $formated_result[$result['categoryId']]['answer_data']   = array();
                  //  $formated_result[$result['categoryId']]['multi_image']   = array();
                }
                
               
                

                $tmp_formated_result = array('answer_id'=>$result['id'],
                    'subCategoryId'=>$result['subCategoryId'],
                    'questionsId'=>$result['questionsId'],
                    'answerType'=>$result['answerType'],
                    'numberOfSample'=>$result['numberOfSample'],
                    'totalSample'=>$result['totalSample'],
                    'totalSampleValues'=>$result['totalSampleValues'],
                    'comments'=>$result['comments'],
                    'remark'=>$result['remark'],
                    'image'=>$result['image'],
                    'action'=>$result['action'],
                    'auditsId'=>$result['auditsId'],
                    'ques_skip'=>$result['ques_skip'],
                    'type'=>$result['type'],
                    'categoryName'=>$result['categoryName'],
                    'name'=>$result['name'],
                    
                    'questionText'=>$result['questionText'],
                 );
                
                array_push($formated_result[$result['categoryId']]['answer_data'], $tmp_formated_result);
            } //die();
      }
      return array_values($formated_result);
}

function filter_answers2($results)
    {

        $formated_result = array();
        if (!empty($results))
        {
            foreach ($results as $result)
            {  // pr($result);

                $ndata =array();
                if (!array_key_exists($result['categoryId'], $formated_result))
                {
                    $formated_result[$result['categoryId']]      = array();
                    $formated_result[$result['categoryId']]['categoryId']   = $result['categoryId'];
                    $formated_result[$result['categoryId']]['categoryName']   = $result['categoryName'];
                    $formated_result[$result['categoryId']]['cat_skip']   = $result['cat_skip'];
                    $formated_result[$result['categoryId']]['status']   = $result['status'];
                    $formated_result[$result['categoryId']]['show_to_client']   = $result['show_to_client'];
                    $formated_result[$result['categoryId']]['get_report_in']   = $result['get_report_in'];
                    $formated_result[$result['categoryId']]['auditsId']   = $result['auditsId'];
                    $formated_result[$result['categoryId']]['startdateTime']   = $result['startdateTime'];
                    $formated_result[$result['categoryId']]['enddatetime']   = $result['enddatetime'];
                    $formated_result[$result['categoryId']]['previous_percentage']   = $result['previous_percentage'];
                    $formated_result[$result['categoryId']]['updatedBy']   = $result['updatedBy'];
                    $formated_result[$result['categoryId']]['answer_data']   = array();
                  //  $formated_result[$result['categoryId']]['multi_image']   = array();
                }
                
               
                

                $tmp_formated_result = array('answer_id'=>$result['id'],
                    'subCategoryId'=>$result['subCategoryId'],
                    'questionsId'=>$result['questionsId'],
                    'answerType'=>$result['answerType'],
                    'numberOfSample'=>$result['numberOfSample'],
                    'totalSample'=>$result['totalSample'],
                    'totalSampleValues'=>$result['totalSampleValues'],
                    'comments'=>$result['comments'],
                    'remark'=>$result['remark'],
                    'image'=>$result['image'],
                    'action'=>$result['action'],
                    'auditsId'=>$result['auditsId'],
                    'ques_skip'=>$result['ques_skip'],
                    'type'=>$result['type'],
                    'categoryName'=>$result['categoryName'],
                    'name'=>$result['name'],
                    
                    'questionText'=>$result['questionText'],
                 );

                // $ndata[$result['categoryId']][$result['subCategoryId']][] = $tmp_formated_result;
                $formated_result[$result['categoryId']]['answer_data'][$result['subCategoryId']][] = $tmp_formated_result;
                // array_push($formated_result[$result['categoryId']]['answer_data'], $tmp_formated_result);
            }
      }

      $formated_result_arr = array_values($formated_result);
      if($formated_result_arr)
      {
          foreach ($formated_result_arr as $fkey => $fvalue) 
          {
            if(isset($fvalue['answer_data']) && count($fvalue['answer_data']))
            {
                $raw_data = array();
                foreach ($fvalue['answer_data'] as $akey => $avalue) 
                {
                    if($avalue)
                    {
                        foreach ($avalue as $kkey => $kvalue) 
                        {
                            $raw_data[] = $kvalue;
                        }
                    }
                }   
                $formated_result_arr[$fkey]['answer_data'] =  $raw_data;              
            }
          }
      }
      return $formated_result_arr;
}
 
 function filter_report($results)
    {
     
    
        $formated_result = $test_array = array();
        if (!empty($results))
        {
            foreach ($results as $result)
            {
//                 pr($result['categoryName']);
//           die();
                if (!array_key_exists($result['subCatId'], $test_array))
                {
                    $test_array[$result['subCatId']]= $result['subCatId'];
                    $formated_result['info']      = array();
                    $formated_result['info']['audit_contact']   = $result['audit_contact'];
                    $formated_result['info']['time_in']   = $result['time_in'];
                    $formated_result['info']['time_out']   = $result['time_out'];
                    $formated_result['info']['date']   = $result['date'];
                    $formated_result['info']['auditor_name']   = $result['auditor_name'];
                    $formated_result['info']['email']   = $result['email'];
                    $formated_result['info']['storeName']   = $result['storeName'];
                    $formated_result['info']['manager_name']   = $result['manager_name'];
                    $formated_result['info']['region']   = $result['region'];
                    $formated_result['info']['audit_sign']   = $result['audit_sign'];
                    $formated_result['info']['status']   = $result['status'];
                    //$formated_result[$result['subCatId']]      = array();
                    
                    
                    switch ($result['name']) 
                    {
                        /*case "BE CLEAN BE HEALTHY":
                            $name='be_clean';
                            break;
                        case "KEEP IT COLD KEEP IT HOT":
                            $name='keep_it';
                            break;
                        case "Wash Rinse Sanitize":
                            $name='wash_rinse';
                            break;
                        case "DON'T CROSS CONTAMINATE":
                            $name='do_not_cross';
                            break;
                        case "COOK IT CHILL IT":
                            $name='cook_it';
                            break;
                        case "PRODUCT QUALITY & SAFETY":
                            $name='product';
                            break;
                        case "INTEGRATED PEST MANAGEMENT":
                           $name='integrated';
                            break;
                        case "L&C":
                            $name='l_and_c';
                            break;
                         default :
                            $name = $result['name'];*/
                        case "L&C":
                            $name='l_and_c';
                            break;
                        case "BE CLEAN BE HEALTHY":
                            $name='be_clean';
                            break;
                        case "KEEP IT COLD KEEP IT HOT":
                            $name='keep_it';
                            break;
                        case "Wash Rinse Sanitize":
                            $name='wash_rinse';
                            break;
                        case "DON'T CROSS CONTAMINATE":
                            $name='do_not_cross';
                            break;
                        case "COOK IT CHILL IT":
                            $name='cook_it';
                            break;
                        case "PRODUCT QUALITY & SAFETY":
                            $name='product';
                            break;
                        case "INTEGRATED PEST MANAGEMENT":
                           $name='integrated';
                            break;
                        case "L&C":
                            $name='l_and_c';
                            break;
                         default :
                            $name = $result['name'];
                    }
                    $formated_result[$name]   = array();
                }
                $categoryName = array();
                    switch ($result['categoryName']) 
                    {
                        case "Meat Shop":
                            $categoryName='Meat Shop';
                            break;
                        case "Bakery":
                            $categoryName='Bakery';
                            break;
                        case "Fruits & Vegetables":
                            $categoryName='FnV';
                            break;
                        case "Grocery":
                            $categoryName='GROCERY';
                            break;
                        case "Back Room & Receiving Area":
                            $categoryName='Back Room';
                            break;
                        case "Fruits and Vegetables Processing":
                            $categoryName='FnV P';
                            break;
                        case "Licence and Conditions":
                           $categoryName='LnC';
                            break;
                         default :
                            $categoryName = $result['categoryName'];
                    }
                
                $tmp_formated_result = array('comments'=>$result['comments'],
                    'remark'=>$result['remark'],
                    'image'=>$result['image'],
                    'categoryName'=>$categoryName);
                array_push($formated_result[$name], $tmp_formated_result);
            }
      }
        return $formated_result;
    }

  function filter_report_remark2($results)
  {
    $pdf_report_buckets = array();

    if($results)
    {
        foreach ($results as $key => $value) 
        {
            //if(!empty($value['remark']))
            //{
                $bucket_question = array(
                    'comments'=>$value['comments'],
                    'remark'=>$value['remark'],
                    'image'=>$value['image'],
                    'categoryName'=>$value['categoryName'],
                    'subcat_order'=>$value['subcat_order'],
                    'name'=>$value['name'],
                    'subcat_image'=>$value['subcat_image'],
                    'id'=>$value['id']
                );

                $pdf_report_buckets[$value['subCategoryId']][] = $bucket_question;
           // }
        }
    }

    return $pdf_report_buckets;

  }    
	
  function filter_report_remark($results)
    { //  pr($results);die();
        $formated_result = $test_array = array();
        // $i='';
        if (!empty($results))
        {  
            $i='';
            foreach ($results as $result)
            {
                if (!array_key_exists($result['subCategoryId'], $test_array))
                { //$i=0;
                    $test_array[$result['subCategoryId']]= $result['subCategoryId'];
                   // pr($result['name']); die();
                    switch (trim($result['name'])) 
                    {
//                        case "BE CLEAN BE HEALTHY":
//                            $name='be_clean';
//                            break;
                        default :
                            $name = $result['name'];
                            
                    }
                    $formated_result[$i+1]=array();
                   // $formated_result[$i]   = array();
                    $i=$i+1;
                }
//                switch(trim($result['questionText'])){
//                    default :
//                        $question = $result['questionText'];
//                }
                    switch (trim($result['categoryName'])) 
                    {
//                        case "Meat Shop":
//                            $categoryName='Meat Shop';
//                            break;
//                        case "Bakery":
//                            $categoryName='Bakery';
//                            break;
//                        case "Fruits & Vegetables":
//                            $categoryName='FnV';
//                            break;
//                        case "Grocery":
//                            $categoryName='Grocery';
//                            break;
//                        case "Back Room & Receiving Area":
//                            $categoryName='Back Room';
//                            break;
//                        case "Fruits and Vegetables Processing":
//                            $categoryName='FnV P';
//                            break;
//                        case "Licence and Conditions":
//                           $categoryName='LnC';
//                            break;
                        default :
                            $categoryName = $result['categoryName'];
                    }
                if(empty($result['remark']))
					continue;
                $tmp_formated_result = array('comments'=>$result['comments'],
                    'remark'=>$result['remark'],
                    'image'=>$result['image'],
                    'categoryName'=>$categoryName,
                    'subcat_order'=>$result['subcat_order'],
                    'name'=>$name,
                    'subcat_image'=>$result['subcat_image'],
                    'id'=>$result['id']);
                array_push($formated_result[$i], $tmp_formated_result);
            }
      }
        return $formated_result;
    }
    
	
    
    
 function filter_report_no_exception($results)
    {   
     
        $formated_result = $test_array = array();
        // $i='';
        if (!empty($results))
        {  
            $i='';
            foreach ($results as $result)
            {
                if (!array_key_exists($result['subCategoryId'], $test_array))
                {  
                   
                    //$i=0;
                    $test_array[$result['subCategoryId']]= $result['subCategoryId'];
                   // pr($result['name']); die();
                    switch (trim($result['name'])) 
                    {
//                        case "BE CLEAN BE HEALTHY":
//                            $name='be_clean';
//                            break;
                        default :
                            $name = $result['name'];
                            
                    }
                    $formated_result[$i+1]=array();
                   // $formated_result[$i]   = array();
                    $i=$i+1;
                    
                }
//                switch(trim($result['questionText'])){
//                    default :
//                        $question = $result['questionText'];
//                }
                    switch (trim($result['categoryName'])) 
                    {
//                        case "Meat Shop":
//                            $categoryName='Meat Shop';
//                            break;
//                        case "Bakery":
//                            $categoryName='Bakery';
//                            break;
//                        case "Fruits & Vegetables":
//                            $categoryName='FnV';
//                            break;
//                        case "Grocery":
//                            $categoryName='Grocery';
//                            break;
//                        case "Back Room & Receiving Area":
//                            $categoryName='Back Room';
//                            break;
//                        case "Fruits and Vegetables Processing":
//                            $categoryName='FnV P';
//                            break;
//                        case "Licence and Conditions":
//                           $categoryName='LnC';
//                            break;
                        default :
                            $categoryName = $result['categoryName'];
                    }
                
//                if(!empty($result['remark']))
//					continue; 
                $tmp_formated_result = array('comments'=>$result['comments'],
                    'remark'=>$result['remark'],
                    'image'=>$result['image'],
                    'categoryName'=>$categoryName,
                    'subcat_order'=>$result['subcat_order'],
                    'name'=>$name,
                    'subcat_image'=>$result['subcat_image'],
                    'id'=>$result['id']);
                
               
                array_push($formated_result[$i], $tmp_formated_result);
            }
      }
     
        return $formated_result;
    }
	
   function filter_report_excelsheet($results)
    { //  pr($results);die();
        $formated_result = $test_array = array();
        // $i='';
        if (!empty($results))
        {  
            $i='';
            foreach ($results as $result)
            {
                if (!array_key_exists($result['subCategoryId'], $test_array))
                { //$i=0;
                    $test_array[$result['subCategoryId']]= $result['subCategoryId'];
                   // pr($result['name']); die();
                    switch (trim($result['name'])) 
                    {
//                        case "BE CLEAN BE HEALTHY":
//                            $name='be_clean';
//                            break;
                        default :
                            $name = $result['name'];
                            
                    }
                    $formated_result[$i+1]=array();
                   // $formated_result[$i]   = array();
                    $i=$i+1;
                }
//                switch(trim($result['questionText'])){
//                    default :
//                        $question = $result['questionText'];
//                }
                    switch (trim($result['categoryName'])) 
                    {
//                        case "Meat Shop":
//                            $categoryName='Meat Shop';
//                            break;
//                        case "Bakery":
//                            $categoryName='Bakery';
//                            break;
//                        case "Fruits & Vegetables":
//                            $categoryName='FnV';
//                            break;
//                        case "Grocery":
//                            $categoryName='Grocery';
//                            break;
//                        case "Back Room & Receiving Area":
//                            $categoryName='Back Room';
//                            break;
//                        case "Fruits and Vegetables Processing":
//                            $categoryName='FnV P';
//                            break;
//                        case "Licence and Conditions":
//                           $categoryName='LnC';
//                            break;
                        default :
                            $categoryName = $result['categoryName'];
                    }
                if(empty($result['remark']))
					continue;
                $tmp_formated_result = array('comments'=>$result['comments'],
                    'remark'=>$result['remark'],
                    'image'=>$result['image'],
                    'categoryName'=>$categoryName,
                    'name'=>$name,
                    'subcat_image'=>$result['subcat_image'],
                    'auditid'=>$result['auditsId'],
                    'id'=>$result['id']);
                array_push($formated_result[$i], $tmp_formated_result);
            }
      }
        return $formated_result;
    } 
    
    
     function filter_report_no_exception_excelsheet($results)
    {   //pr($results);die();
        $formated_result = $test_array = array();
        // $i='';
        if (!empty($results))
        {  
            $i='';
            foreach ($results as $result)
            {
                if (!array_key_exists($result['subCategoryId'], $test_array))
                { //$i=0;
                    $test_array[$result['subCategoryId']]= $result['subCategoryId'];
                   // pr($result['name']); die();
                    switch (trim($result['name'])) 
                    {
//                        case "BE CLEAN BE HEALTHY":
//                            $name='be_clean';
//                            break;
                        default :
                            $name = $result['name'];
                            
                    }
                    $formated_result[$i+1]=array();
                   // $formated_result[$i]   = array();
                    $i=$i+1;
                }
//                switch(trim($result['questionText'])){
//                    default :
//                        $question = $result['questionText'];
//                }
                    switch (trim($result['categoryName'])) 
                    {
//                        case "Meat Shop":
//                            $categoryName='Meat Shop';
//                            break;
//                        case "Bakery":
//                            $categoryName='Bakery';
//                            break;
//                        case "Fruits & Vegetables":
//                            $categoryName='FnV';
//                            break;
//                        case "Grocery":
//                            $categoryName='Grocery';
//                            break;
//                        case "Back Room & Receiving Area":
//                            $categoryName='Back Room';
//                            break;
//                        case "Fruits and Vegetables Processing":
//                            $categoryName='FnV P';
//                            break;
//                        case "Licence and Conditions":
//                           $categoryName='LnC';
//                            break;
                        default :
                            $categoryName = $result['categoryName'];
                    }
                if(!empty($result['remark']))
					continue;
                $tmp_formated_result = array('comments'=>$result['comments'],
                    'remark'=>$result['remark'],
                    'image'=>$result['image'],
                    'categoryName'=>$categoryName,
                    'name'=>$name,
                    'subcat_image'=>$result['subcat_image'],
                    'auditidscore'=>$result['auditidscore'],
                    'id'=>$result['id']);
                
               
                array_push($formated_result[$i], $tmp_formated_result);
            }
      }
        return $formated_result;
    }
    
	
function filter_overallRating($results)
{
    $total = 0;
    $totalSampleValues = 0;
    foreach($results as $result)
    {
        // 1=Yes, 2=No, 3=Partial
        if($result['answerType']==1)
        {
            $totalSampleValues = $totalSampleValues + $result['numberOfSample']*10;
            $total = $total + $result['numberOfSample']*10;
        }else if($result['answerType']==2)
        {
            $totalSampleValues = $totalSampleValues + $result['numberOfSample']*0;
            $total = $total + $result['numberOfSample']*10;  
        }
        else if($result['answerType']==3)
        {
            $totalSampleValues = $totalSampleValues+ $result['totalSampleValues'];
            $total = $total + $result['numberOfSample']*10; 
        }
    }
//    echo $totalSampleValues;
//    echo '<br/>';
//    echo $total;
//    die;
    if($total==0)
        $rating = 0;
    else
        $rating = number_format(($totalSampleValues*100)/$total, 2, '.', '');
    return $rating;
}

//function filter_Ratings($results)
//{
//    $total_complaince = $obtain_complaince =0;
//    $total_non_complaince = $obtain_non_complaince = 0;
//    $total_partial = $obtain_partial = 0;
//    
//    foreach($results as $result)
//    {
//        // 1=Yes, 2=No, 3=Partial
//        if($result['answerType']==1)
//        {
//            $obtain_complaince = $obtain_complaince + $result['numberOfSample']*10;
//            $total_complaince = $total_complaince + $result['numberOfSample']*10;
//        }else if($result['answerType']==2)
//        {
//            $obtain_non_complaince = $obtain_non_complaince + $result['numberOfSample']*0;
//            $total_non_complaince = $total_non_complaince + $result['numberOfSample']*10;  
//        }
//        else if($result['answerType']==3)
//        {
//            $obtain_partial = $obtain_partial+ $result['totalSampleValues'];
//            $total_partial = $total_partial + $result['numberOfSample']*10; 
//        }
//    }
//    $result = array('obtain_complaince' => $obtain_complaince,
//        'total_complaince' => $total_complaince,
//        'obtain_non_complaince' => $obtain_non_complaince,
//        'total_non_complaince' => $total_non_complaince,
//        'obtain_partial' => $obtain_partial,
//        'total_partial' => $total_partial,
//        
//        
//        );
//    return $result;
//}

function filter_categoryWiseRating($results)
{
    $rating=$total=$totalSampleValues = 0;
    $temp_array =$formated_result= array();
    
    foreach($results as $result)
    {
        if (!array_key_exists($result['id'], $formated_result))
        {
            $rating = $totalSampleValues = $total =0;
            $formated_result[$result['id']]      = array();
            $formated_result[$result['id']]['id']   = $result['id'];
            $formated_result[$result['id']]['categoryName']   = $result['categoryName'];
            $formated_result[$result['id']]['data']   = array();
        }
         // 1=Yes, 2=No, 3=Partial
        if($result['answerType']==1)
        {
            $totalSampleValues = $result['numberOfSample']*10;
            $total =  $result['numberOfSample']*10;  
        }else if($result['answerType']==2)
        {
            $totalSampleValues =  0;
            $total = $result['numberOfSample']*10;  
        }
        else if($result['answerType']==3)
        {
            $totalSampleValues = $result['totalSampleValues'];
            $total = $result['numberOfSample']*10; 
        }
        $temp_array = array('total'=>$total,'totalSampleValues'=>$totalSampleValues);
        array_push($formated_result[$result['id']]['data'],$temp_array);
    }
    return $formated_result;
}

function filter_complianceReport($results)
{
   
    $formated_result = $temp_array = array();
    if (!empty($results))
    {
        foreach ($results as $result)
        {
            if (!array_key_exists($result['categoryId'], $formated_result))
            {
                $formated_result[$result['categoryId']]      = array();
                $formated_result[$result['categoryId']]['categoryId']   = $result['categoryId'];
                $formated_result[$result['categoryId']]['categoryName']   = $result['categoryName'];
                $formated_result[$result['categoryId']]['subCategory']   = array();
            }
            
            if(!array_key_exists($result['subCategoryId'], $formated_result[$result['categoryId']]['subCategory'])){
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['subCategoryId'] = $result['subCategoryId'];
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['subCategoryName'] = $result['name'];
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['categoryName'] = $result['categoryName'];
                 $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['question'] = array();
            }
            
            array_push($formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['question'],array('ques'=>$result['questionText'],'numberOfSample'=>$result['numberOfSample'],'totalSampleValues'=>$result['totalSampleValues'],'remark'=>$result['remark'],'image'=>$result['image']));
            
        }
  }
  // pr($formated_result);die;
  //$result_array = filter_subCat($formated_result);
  return array_values($formated_result);
 }
 
 function filter_partialReport($results)
{
   
    $formated_result = $temp_array = array();
    if (!empty($results))
    {
        foreach ($results as $result)
        {
            if (!array_key_exists($result['categoryId'], $formated_result))
            {
                $formated_result[$result['categoryId']]      = array();
                $formated_result[$result['categoryId']]['categoryId']   = $result['categoryId'];
                $formated_result[$result['categoryId']]['categoryName']   = $result['categoryName'];
                $formated_result[$result['categoryId']]['subCategory']   = array();
            }
            
            if(!array_key_exists($result['subCategoryId'], $formated_result[$result['categoryId']]['subCategory'])){
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['subCategoryId'] = $result['subCategoryId'];
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['subCategoryName'] = $result['name'];
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['categoryName'] = $result['categoryName'];
                 $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['question'] = array();
            }
            
            array_push($formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['question'],array('ques'=>$result['questionText'],'numberOfSample'=>$result['numberOfSample'],'totalSampleValues'=>$result['totalSampleValues'],'remark'=>$result['remark'],'image'=>$result['image']));
            
        }
  }
  // pr($formated_result);die;
  //$result_array = filter_subCat($formated_result);
  return array_values($formated_result);
 }
 
 
 function filter_complianceReport_backup($results)
{
   
    $formated_result = $temp_array = array();
    if (!empty($results))
    {
        foreach ($results as $result)
        {
            if (!array_key_exists($result['categoryId'], $formated_result))
            {
                $formated_result[$result['categoryId']]      = array();
                $formated_result[$result['categoryId']]['categoryId']   = $result['categoryId'];
                $formated_result[$result['categoryId']]['categoryName']   = $result['categoryName'];
                $formated_result[$result['categoryId']]['subCategory']   = array();
            }
            
            if(!array_key_exists($result['subCategoryId'], $formated_result[$result['categoryId']]['subCategory'])){
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['subCategoryId'] = $result['subCategoryId'];
                $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['subCategoryName'] = $result['name'];
                 $formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['question'] = array();
            }
            
            array_push($formated_result[$result['categoryId']]['subCategory'][$result['subCategoryId']]['question'],$result['questionText']);
            
        }
  }
  // pr($formated_result);die;
  //$result_array = filter_subCat($formated_result);
  return array_values($formated_result);
 }

 
 function questionType($type)
 {
     if($type==1)
         echo 'Value';
     else
         echo 'No value';
 }
 
 
?>
