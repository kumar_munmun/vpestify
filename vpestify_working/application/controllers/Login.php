<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		 parent::__construct();
		 $this->load->helper('form');
		 $this->load->library('image_lib');
		 $this->load->library('upload');
		 $this->load->helper('url');
		 $this->load->model('Login_model');
		 $this->load->helper(array('captcha'));
		 $this->load->helper("file");
// Load the captcha helper
        $this->load->helper('captcha');		 
	}
	
	/*---------------------------------Login Admin Page-------------------------*/
	
	public function login()
	{
		
		if(isset($_POST['btn_submit']))
		{
			$time	=	$_POST['iname'];
			$path	=	base_url()."static/".$time;

			if(file_exists(FCPATH . "/static/" .$time))
			{
				unlink( FCPATH . "/static/" .$time);
			}
			
			//delete_files($path);
			$word	=	$_POST['word'];
			$eword	=	$_POST['code'];
			if($word==$eword)
			{
				$query=$this->Login_model->mloginAdmin();
			if($query=='true')
			{  
               redirect('Login/accessModule');
            }else
			{      $data	=	array(
					'img_path'=>'./static/',
					'img_url'=>base_url()."/static/",
					'img_width'=>'150',
					'img_height'=>'30'
					);
					$captcha	=	create_captcha($data);
					$captcha["login_failure"] = "Entered User Name or Password is incorrect";
					$this->load->view('home_view',$captcha); 
            		
			}
			}
			else{
			/* include("securimage/securimage.php");
			$img 	= new Securimage();
			$code	=	$_POST['code'];
			$valid = $img->check($code);
			if($valid == '') {
			
			$query=$this->Login_model->mloginAdmin();
			if($query=='true')
			{  
               redirect('Login/accessModule');
            }else{            
                	$data["login_failure"] = "Entered User Name or Password is incorrect";
					$this->load->view('home_view',$data); 
            	} 	
			
			}	else	{
				
				redirect( base_url());
				
			}
			
			
		}	
		else { $this->load->view('home_view'); } */
		$data	=	array(
		'img_path'=>'./static/',
		'img_url'=>base_url()."/static/",
		'img_width'=>'150',
		'img_height'=>'30'
		);
		$captcha	=	create_captcha($data);
		$captcha["login_failure"] = "Entered Captcha is wrong";
		$this->load->view('home_view',$captcha);
		}
	}
	
}
	public function accessModule()
	{
		if($_SESSION['main_modulemasterid']=='')
		{
			$_SESSION['main_modulename']		=	$_SESSION['modulename'];		 
			$_SESSION['main_modulemasterid']	=	$_SESSION['modulemasterid'];
			$_SESSION['main_rolemasterid']		=	$_SESSION['rolemasterid'];
			$_SESSION['main_rolename']			=	$_SESSION['rolename'];
			$_SESSION['main_academymasterid']	=	$_SESSION['academymasterid'];
			$_SESSION['main_academyname']		=	$_SESSION['academyname'];
		}
		
		if($_SESSION['modulename']=='user')
		{
			redirect('Usermodule/userModule');
		}
		else
		   {		   	  				  
				$this->load->view('access_module_view');
		   }
	}
	
	public function switchModule()
	{
		$_SESSION['main_modulename']		=	$_SESSION['modulename'];		 
		$_SESSION['main_modulemasterid']	=	$_SESSION['modulemasterid'];
		$_SESSION['main_rolemasterid']		=	$_SESSION['rolemasterid'];
		$_SESSION['main_rolename']			=	$_SESSION['rolename'];
		$_SESSION['main_academymasterid']	=	$_SESSION['academymasterid'];
		$_SESSION['main_academyname']		=	$_SESSION['academyname'];
		unset($_SESSION['academic_year']);
		
		$this->load->view('access_module_view');
		
	}
}
?>