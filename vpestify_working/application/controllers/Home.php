<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');	
		$this->load->model('Home_model');			
		if ( function_exists( 'date_default_timezone_set' ) )
				date_default_timezone_set('Asia/Kolkata');
		 
	}
	public function index()	{	
		$getqry	=	$this->Home_model->mgetImageOnHome();
		if($getqry->num_rows()>0){
			$data['res_set']	=	$getqry->result();
		}
		else{
				$data['error']	=	"Not Found";
			}
		
		$qry	=	$this->Home_model->mgetImagetypeContent();
		if($qry->num_rows()>0)	{
			$data['rsType']	=	$qry->result();
		}
		
		$qryService	=	$this->Home_model->mgetPestServices();
		if($qryService->num_rows()>0){
			$data['rsServices']	=	$qryService->result();
		}
		
		$this->load->view('layouts/home_view', $data);		
    }
	
}
