<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Avservices extends CI_Controller {

    public function __construct() {
	parent::__construct();
	$this->load->library('session');
	$this->load->helper('url');
	$this->load->model('Home_model');
	$this->load->model('Services_model');
	if (function_exists('date_default_timezone_set')) date_default_timezone_set('Asia/Kolkata');
    }

    /* ---------------------------------------AV Global Contact Us--------------- */

    public function contactUs() {
	$qryService = $this->Home_model->mgetPestServices();
	if ($qryService->num_rows() > 0) {
	    $data['rsServices'] = $qryService->result();
	}
	$getImg = $this->Services_model->mgetPestServiceImageTop('contact');	
	if ($getImg->num_rows() > 0) {
	    $data['service_img'] = $getImg->row();
	} else {
	    $data['error'] = "Not Found";
	}

	$this->load->view('layouts/contact_us_view', $data);
    }

    /* --------------------------------------Global About Us--------------------- */

    public function aboutUs() {
	$qryService = $this->Home_model->mgetPestServices();
	if ($qryService->num_rows() > 0) {
	    $data['rsServices'] = $qryService->result();
	}

	$getqry = $this->Services_model->mgetAboutUs();
	if ($getqry->num_rows() > 0) {
	    $data['result_set'] = $getqry->row();
	} else {
	    $data['error'] = "Not Found";
	}
	$getImg = $this->Services_model->mgetPestServiceImageTop('About');	
	if ($getImg->num_rows() > 0) {
	    $data['service_img'] = $getImg->row();
	} else {
	    $data['error'] = "Not Found";
	}
	$this->load->view('layouts/about_us_view', $data);
    }

    /* ----------------------------------------Global Services-------------------------- */

    public function vPestifyServices() {
	if (!$this->uri->segment(2)) {
	    redirect('pestcontrol');
	}
	$getqry = $this->Services_model->mgetPestServices();
	if ($getqry->num_rows() > 0) {
	    $data['service_set'] = $getqry->row();
	} else {
	    $data['error'] = "Not Found";
	}
	
	$getImg = $this->Services_model->mgetPestServiceImageTop($this->uri->segment(2));	
	if ($getImg->num_rows() > 0) {
	    $data['service_img'] = $getImg->row();
	} else {
	    $data['error'] = "Not Found";
	}

	$qryService = $this->Home_model->mgetPestServices();
	if ($qryService->num_rows() > 0) {
	    $data['rsServices'] = $qryService->result();
	}

	$this->load->view('layouts/service_view', $data);
    }

    /* ----------------------------------------Global Services-------------------------- */

    public function vSafety() {
        $qryService = $this->Home_model->mgetPestServices();
    	if ($qryService->num_rows() > 0) {
    	    $data['rsServices'] = $qryService->result();
    	}
    	$getImg = $this->Services_model->mgetPestServiceImageTop('safety');	
    	if ($getImg->num_rows() > 0) {
    	    $data['service_img'] = $getImg->row();
    	} else {
    	    $data['error'] = "Not Found";
    	}
	$this->load->view('layouts/safety_view', $data);
    }

}
