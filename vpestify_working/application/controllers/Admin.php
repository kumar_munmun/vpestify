<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');		
		$this->load->helper('form');	
		if ( function_exists( 'date_default_timezone_set' ) )
				date_default_timezone_set('Asia/Kolkata');
			
		$this->load->model('Adminpanel_model');		
		 
	}
	
	public function AdminGetEnquiryDetails() {
		$get_data	=	$this->Adminpanel_model->mAdminGetEnquiryDetails();
		if($get_data->num_rows()>0) {
			$data['result_set']		=	$get_data->result();
		}
		else {
			$data["error"]	=	"Data is empty";
		}
		
		$this->load->view("admin_layout/Admin_get_enquiryDetails_view", $data);	
		
	}
	/*public function Logout()
	{
		$this->session->sess_destroy();
      	redirect('/Admin/adminLogin');
	}*/
	
	
	public function adminLogin() {	
		$this->load->view('admin_layout/admin_login_view');
    }
	
	public function LoginAuthentic() {
		$login_qry	=	$this->Adminpanel_model->mLoginAuthentic();
		
		if($login_qry=="LoginValid") {	
			
			redirect('Admin/ImageType');
		}
		elseif($login_qry=="LoginInvalid"){
				redirect('Admin/adminLogin');
		}
	}
	
	/*--------------------User Send Enquiry Details--------------*/
	
	public function userSendEnquiry()
	{
		$send_qry	=	$this->Adminpanel_model->muserSendEnquiryDetails();
		if($send_qry=='Ok')
		{
			echo "Ok";
		}
	}
	
	/*---------------Services Image----------------------------*/
	
	public function deleteServiceData()
	{
		$Services_Id	=	$this->uri->segment(3); 
		$del_qry	=	$this->Adminpanel_model->mdeleteServiceData($Services_Id);
		
		redirect('Admin/insertServices');
	}
	
	public function insertServices()
	{
		/*if(!isset($_SESSION['admin']))
		{
			redirect('Admin/adminLogin');
		}*/
		
		//Insert----------
		if(isset($_POST['addData']))
		{
			$chk_query	=	$this->Adminpanel_model->mchkDuplicateService();
			if($chk_query->num_rows()>0)
			{
				 $this->session->set_flashdata('err_msg', '! Data already exists');
				 redirect('Admin/insertServices');
			}
			else{			
					$insert_query	=	$this->Adminpanel_model->minsertServicesData();
					if($insert_query=="Ok")
					{
						$this->session->set_flashdata('msg', 'Data inserted');						
						redirect('Admin/insertServices');
					}
				}					
		}
		
		//Update----------
		
		if(isset($_POST['updateData']))
		{
			$Services_Id    			 =	    trim(FILTER_VAR($this->input->post('Services_Id'),FILTER_SANITIZE_STRING)); 
			$chk_query	=	$this->Adminpanel_model->mchkDuplicateEditModeService($Services_Id);
			if($chk_query->num_rows()>0)
			{
				 $this->session->set_flashdata('err_msg', '! Data already exists');
				 redirect('Admin/insertServices');
			}
			else{
					$insert_query	=	$this->Adminpanel_model->mupdateServicesData();
					if($insert_query=="Ok")
					{
						$this->session->set_flashdata('msg', 'Data updated');
						redirect('Admin/insertServices');
					}
				}	
		}
		
		$result_set	=	$this->Adminpanel_model->mgetServicesDetails();
		if($result_set->num_rows()>0)
		{
			$data['services']		=		$result_set->result();
		}
		else	{
			$data['Error']			=		"! Data not found";
		}
		
		
		$this->load->view('admin_layout/admin_add_services_view', $data);
	}
	
	
	/*----------------Get Image Type Upload Details---------------*/
	
	
	public function deleteImageUpload() {
		$image_id	=	$this->uri->segment(3); 
		$del_qry	=	$this->Adminpanel_model->mdeleteImageUploadData($image_id);
		
		redirect('Admin/ImageUpload');
	}
	
	public function ImageUpload() {
		/*echo $_SESSION['admin']; exit;
		
		if(!isset($_SESSION['admin']))
		{
			redirect('Admin/adminLogin');
		}*/
		
		//Insert----------
		
		if(isset($_POST['addData']))
		{
			$insert_query	=	$this->Adminpanel_model->mimageUploadData();
			if($insert_query=="Ok")
			{
				redirect('Admin/ImageUpload');
			}
		}
		
		//Update----------
		
		if(isset($_POST['updateData']))
		{
			$insert_query	=	$this->Adminpanel_model->mupdateImageUploadData();
			if($insert_query=="Ok")
			{
				redirect('Admin/ImageUpload');
			}
		}
		
		$qrySet		=	$this->Adminpanel_model->mgetImageUploadData();
		if($qrySet->num_rows()>0)
		{
			$data['imageUpload']		=		$qrySet->result();
		}
				
		$result_set	=	$this->Adminpanel_model->mgetImageTypeDetails();
		if($result_set->num_rows()>0)
		{
			$data['imageType']		=		$result_set->result();
		}
		else	{
			$data['Error']			=		"! Data not found";
		}
			$this->load->view('admin_layout/admin_image_upload_view', $data);	
	}
	
	/*----------------Get Image Type Details---------------*/
		
	public function ImageType()
	{			
		/*if(!isset($_SESSION['admin']))
		{
			redirect('Admin/adminLogin');
		}*/
		
		$this->load->view('admin_layout/admin_image_type_view');
	}
		
	public function GetImageTypeDetails()
	{
		$result_set	=	$this->Adminpanel_model->mgetImageTypeDetails();	
		 $i=0;
		 if($result_set)
		 {
			foreach($result_set->result() as $row)
			{
				$arr_data['records'][] 	   =  $row;						
			}
			
                $arr_data['chk_status'][]    =  'OK';	
		 }
			  echo json_encode($arr_data);
	}
	
	// Update Image Type Data-------------------
	
	public function UpdateImageType()
	{
		$update_query	=	$this->Adminpanel_model->mupdateImageTypeDetails();	
		if($update_query)
		{
			$arr_data['chk_status'][]    =  'OK';				
			echo json_encode($arr_data);
		}else
			{
				$arr_data['chk_status'][]    =  'error';
				echo json_encode($arr_data);
			}
	}
	
	/*--------------Delete Image Type data-----------*/
	
	public function deleteImageType()
	{
		 $Image_Type_Id		=	$_POST['Image_Type_Id']; 
		// print_r($Image_Type_Id); exit;
		 
		$delete_query	=	$this->Adminpanel_model->mdeleteImageTypeDetails($Image_Type_Id);	
		if($delete_query)
		{
			$arr_data['chk_status'][]    =  'OK';				
			echo json_encode($arr_data);
		}else
			{
				$arr_data['chk_status'][]    =  'error';
				echo json_encode($arr_data);
			}
	}
	
	// Insert Data Image Type data---
	
	public function insertImageType()
	{
		$result_query	=	$this->Adminpanel_model->minsertImageTypeDetails();	
		if($result_query)
		{
			$getData					 =	$result_query->row();
			$Image_Type_Id				 =	$getData->Image_Type_Id;
			
			$arr_data['Image_Type_Id']	 =  $Image_Type_Id;	
			
			$arr_data['chk_status']	     =  'OK';				
			echo json_encode($arr_data);
		}else
			{
				$arr_data['chk_status']    =  'error';
				echo json_encode($arr_data);
			}
	}
	
	
}
