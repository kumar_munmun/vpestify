<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpanel_model extends CI_Model {

    public function __construct() {
	parent:: __construct();
	$this->load->database();
	//$this->load->library('session');
    }

    public function mAdminGetEnquiryDetails() {
	return $this->db->query("SELECT * FROM tbl_userenquirydetails tud WHERE tud.IsActive=1 ORDER BY tud.Enquiry_Date DESC");
    }

    /* ----------------Admin Login------------------------- */

    public function mLoginAuthentic() {
	$user = $_POST['user'];
	$pass = $_POST['password'];

	if ($user == 'admin' && $pass == 'admin123') {
	    $_SESSION['admin'] = "AdminLogin";
	    return "LoginValid";
	} else {
	    return "LoginInvalid";
	}
    }

    /* -------------M send User Enquiry Details----------- */

    public function muserSendEnquiryDetails() {
	$user = trim(FILTER_VAR($this->input->post('user'), FILTER_SANITIZE_STRING));
	$email = trim(FILTER_VAR($this->input->post('email'), FILTER_SANITIZE_STRING));
	$contact = trim(FILTER_VAR($this->input->post('contact'), FILTER_SANITIZE_STRING));
	$aboutEnquiry = trim(FILTER_VAR($this->input->post('aboutEnquiry'), FILTER_SANITIZE_STRING));

	if ($user != '' && $email != '' && $contact != '') {
	    $data = array("User_Name" => $user, "User_Email" => $email, "Contact_No" => $contact, "Enquiry_Details" => $aboutEnquiry, "IsActive" => 1);
	    $this->db->insert("tbl_userenquirydetails", $data);
	    return "Ok";
	} else {
	    return "! Somthing wrong";
	}
    }

    /* -------------Mget Image Upload data----------- */

    public function mgetImageUploadData() {
	return $this->db->query("SELECT tip.Image_Id, tip.Image_Type_Id, tim.Image_Type_Name, tim.ImageTypeContent, tip.Image_Title, tip.Upload_Image, 
		 						  tip.Image_Place, tip.Image_Content  FROM tbl_imageupload tip
								  INNER JOIN tbl_imagetype tim ON tim.Image_Type_Id=tip.Image_Type_Id AND tim.IsActive=1
								  WHERE tip.IsActive=1");
    }

    public function mdeleteImageUploadData($image_id) {
	$this->db->where('Image_Id', $image_id);
	$this->db->update('tbl_imageupload', array('IsActive' => 0));
    }

    /* -----------------------Services-----29-08-2017----------------------------- */

    public function mdeleteServiceData($Services_Id) {
	$this->db->where('Services_Id', $Services_Id);
	$this->db->update('tbl_pestservices', array('IsActive' => 0));
    }

    public function mgetServicesDetails() {
	return $this->db->query("SELECT tps.Services_Id, tps.Service_Name, tps.Service_Content, tps.Service_Image from tbl_pestservices tps WHERE tps.IsActive=1");
    }

    public function mchkDuplicateService() {
	$ServiceName = trim(FILTER_VAR($this->input->post('ServiceName'), FILTER_SANITIZE_STRING));
	return $this->db->query("SELECT tps.Services_Id, tps.Service_Name from tbl_pestservices tps WHERE Service_Name = '" . $ServiceName . "' and tps.IsActive=1");
    }

    public function mchkDuplicateEditModeService($Services_Id) {
	$ServiceName = trim(FILTER_VAR($this->input->post('ServiceName'), FILTER_SANITIZE_STRING));

	return $this->db->query("SELECT tps.Services_Id, tps.Service_Name from tbl_pestservices tps WHERE Service_Name = '" . $ServiceName . "' and
								  Services_Id!='" . $Services_Id . "' and tps.IsActive=1");
    }

    public function mupdateServicesData() {
	$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
	$createDateTime = $date->format('Y-m-d H:i:sA');

	$ServiceName = trim(FILTER_VAR($this->input->post('ServiceName'), FILTER_SANITIZE_STRING));
	$ServiceContent = trim(($this->input->post('ServiceContent')));
	$Services_Id = trim(FILTER_VAR($this->input->post('Services_Id'), FILTER_SANITIZE_STRING));

	if ($_FILES['userfile']['name'] != '' || $_FILES['userfile']['name'] > 0) {

	    /* --------------Delete Image from Folder-------------------- */

	    $getQry = $this->db->query("SELECT tip.Services_Id, tip.Service_Image from tbl_pestservices tip WHERE tip.Services_Id='" . $Services_Id . "' ");
	    $getData = $getQry->row();
	    $getImageSource = $getData->Service_Image;
	    unlink(FCPATH . $getImageSource);


	    $image = preg_replace("/\s+/", "_", $_FILES['userfile']['name']);
	    @$extension = end(explode(".", $image));
	    $newfilename = time() . $ServiceName . "." . $extension;
	    $_FILES['userfile']['name'] = $newfilename;

	    $config['upload_path'] = "./ImageService/";
	    $config['allowed_types'] = "gif|jpg|png|jpeg|JPG|JPEG|PNG|GIF";

	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload() == false) {
		$error = array('error' => $this->upload->display_errors());
		echo $error['error'];
	    } else {
		$config['image_library'] = 'gd2';
		$config['source_image'] = './ImageService/' . $newfilename;
		$config['new_image'] = './ImageService/';
		$config['maintain_ratio'] = false;
		$settings['quality'] = '100';

		$config['width'] = 400;
		$config['height'] = 400;
		$this->load->library('image_lib', $config);
		if (!$this->image_lib->resize()) {
		    $this->image_lib->display_errors();
		}

		$imagePath = "ImageService/" . preg_replace("/\s+/", "_", $newfilename);

		$data = array('Service_Name' => $ServiceName, 'Service_Content' => $ServiceContent, 'Service_Image' => $imagePath,
		    'CreatedDate' => $createDateTime, 'IsActive' => 1);

		$this->db->where("Services_Id", $Services_Id);
		$qry = $this->db->update('tbl_pestservices', $data);

		return "Ok";
	    }
	} else {
	    $data = array('Service_Name' => $ServiceName, 'Service_Content' => $ServiceContent, 'CreatedDate' => $createDateTime, 'IsActive' => 1);

	    $this->db->where("Services_Id", $Services_Id);
	    $qry = $this->db->update('tbl_pestservices', $data);

	    return "Ok";
	}
    }

    public function minsertServicesData() {
	$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
	$createDateTime = $date->format('Y-m-d H:i:sA');

	$ServiceName = trim(FILTER_VAR($this->input->post('ServiceName'), FILTER_SANITIZE_STRING));
	$ServiceContent = trim(FILTER_VAR($this->input->post('ServiceContent'), FILTER_SANITIZE_STRING));

	$getQry = $this->db->query("SELECT tip.Services_Id, tip.Service_Image from tbl_pestservices tip WHERE tip.Service_Name='" . $ServiceName . "' 
		 										  and IsActive=0 ");
	if ($getQry->num_rows() > 0) {
	    $getData = $getQry->row();
	    $getImageSource = $getData->Service_Image;
	    $Services_Id = $getData->Services_Id;
	    unlink(FCPATH . $getImageSource);
	}


	$image = preg_replace("/\s+/", "_", $_FILES['userfile']['name']);
	@$extension = end(explode(".", $image));
	$newfilename = time() . $ServiceName . "." . $extension;
	$_FILES['userfile']['name'] = $newfilename;

	$config['upload_path'] = "./ImageService/";
	$config['allowed_types'] = "gif|jpg|png|jpeg|JPG|JPEG|PNG|GIF";

	$this->load->library('upload', $config);

	if ($this->upload->do_upload() == false) {
	    $error = array('error' => $this->upload->display_errors());
	    echo $error['error'];
	} else {
	    $config['image_library'] = 'gd2';
	    $config['source_image'] = './ImageService/' . $newfilename;
	    $config['new_image'] = './ImageService/';
	    $config['maintain_ratio'] = false;
	    $settings['quality'] = '100';

	    $config['width'] = 400;
	    $config['height'] = 400;
	    $this->load->library('image_lib', $config);
	    if (!$this->image_lib->resize()) {
		$this->image_lib->display_errors();
	    }

	    $imagePath = "ImageService/" . preg_replace("/\s+/", "_", $newfilename);

	    if ($getQry->num_rows() > 0) {
		$getData = $getQry->row();
		$Services_Id = $getData->Services_Id;

		$data = array('Service_Name' => $ServiceName, 'Service_Content' => $ServiceContent, 'Service_Image' => $imagePath,
		    'CreatedDate' => $createDateTime, 'IsActive' => 1);

		$this->db->where('Services_Id', $Services_Id);
		$qry = $this->db->update('tbl_pestservices', $data);

		return "Ok";
	    } else {
		$data = array('Service_Name' => $ServiceName, 'Service_Content' => $ServiceContent, 'Service_Image' => $imagePath,
		    'CreatedDate' => $createDateTime, 'IsActive' => 1);

		$qry = $this->db->insert('tbl_pestservices', $data);

		return "Ok";
	    }
	}
    }

    /* --------------Iupdate Image upload Data------------28-08-2018--------------------- */

    public function mupdateImageUploadData() {
	$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
	$createDate_time = $date->format('Y-m-d H:i:sA');

	$ImageTypeId = trim(FILTER_VAR($this->input->post('ImageTypeId'), FILTER_SANITIZE_STRING));
	$ImageTitle = trim(FILTER_VAR($this->input->post('ImageTitle'), FILTER_SANITIZE_STRING));
	$ImagePlace = trim(FILTER_VAR($this->input->post('ImagePlace'), FILTER_SANITIZE_STRING));
	$ImageContent = trim(FILTER_VAR($this->input->post('ImageContent'), FILTER_SANITIZE_STRING));

	$Image_Id = trim(FILTER_VAR($this->input->post('Image_Id'), FILTER_SANITIZE_STRING));

	if ($_FILES['userfile']['name'] != '' || $_FILES['userfile']['name'] > 0) {

	    /* --------------Delete Image from Folder-------------------- */

	    $getQry = $this->db->query("SELECT tip.Image_Id, tip.Upload_Image from tbl_imageupload tip WHERE tip.Image_Id='" . $Image_Id . "' ");
	    $getData = $getQry->row();
	    $getImageSource = $getData->Upload_Image;
	    unlink(FCPATH . $getImageSource);


	    $image = preg_replace("/\s+/", "_", $_FILES['userfile']['name']);
	    @$extension = end(explode(".", $image));
	    $newfilename = time() . $ImageTitle . "." . $extension;
	    $_FILES['userfile']['name'] = $newfilename;

	    $config['upload_path'] = "./ImageUpload/";
	    $config['allowed_types'] = "gif|jpg|png|jpeg|JPG|JPEG|PNG|GIF";

	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload() == false) {
		$error = array('error' => $this->upload->display_errors());
		echo $error['error'];
	    } else {
		$config['image_library'] = 'gd2';
		$config['source_image'] = './ImageUpload/' . $newfilename;
		$config['new_image'] = './ImageUpload/';
		$config['maintain_ratio'] = false;
		$settings['quality'] = '100';

		$config['width'] = 400;
		$config['height'] = 400;
		$this->load->library('image_lib', $config);
		if (!$this->image_lib->resize()) {
		    $this->image_lib->display_errors();
		}

		$imagePath = "ImageUpload/" . preg_replace("/\s+/", "_", $newfilename);
		$data = array('Image_Type_Id' => $ImageTypeId, 'Image_Title' => $ImageTitle, 'Image_Place' => $ImagePlace, 'Image_Content' => $ImageContent,
		    'Upload_Image' => $imagePath, 'CreatedDate' => $createDate_time, 'IsActive' => 1);

		$this->db->where("Image_Id", $Image_Id);
		$qry = $this->db->update('tbl_imageupload', $data);


		return "Ok";
	    }
	} else {
	    $data = array('Image_Type_Id' => $ImageTypeId, 'Image_Title' => $ImageTitle, 'Image_Place' => $ImagePlace, 'Image_Content' => $ImageContent,
		'CreatedDate' => $createDate_time, 'IsActive' => 1);

	    $this->db->where("Image_Id", $Image_Id);
	    $qry = $this->db->update('tbl_imageupload', $data);

	    return "Ok";
	}
    }

    /* ----------------------Image Upload Type Data----26-08-2017------------------------ */

    public function mimageUploadData() {
	$date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
	$createDate_time = $date->format('Y-m-d H:i:sA');

	$ImageTypeId = trim(FILTER_VAR($this->input->post('ImageTypeId'), FILTER_SANITIZE_STRING));
	$ImageTitle = trim(FILTER_VAR($this->input->post('ImageTitle'), FILTER_SANITIZE_STRING));
	$ImagePlace = trim(FILTER_VAR($this->input->post('ImagePlace'), FILTER_SANITIZE_STRING));
	$ImageContent = trim(FILTER_VAR($this->input->post('ImageContent'), FILTER_SANITIZE_STRING));

	$image = preg_replace("/\s+/", "_", $_FILES['userfile']['name']);
	@$extension = end(explode(".", $image));
	$newfilename = time() . $ImageTitle . "." . $extension;
	$_FILES['userfile']['name'] = $newfilename;

	$config['upload_path'] = "./ImageUpload/";
	$config['allowed_types'] = "gif|jpg|png|jpeg|JPG|JPEG|PNG|GIF";

	$this->load->library('upload', $config);

	if ($this->upload->do_upload() == false) {
	    $error = array('error' => $this->upload->display_errors());
	    echo $error['error'];
	} else {
	    $config['image_library'] = 'gd2';
	    $config['source_image'] = './ImageUpload/' . $newfilename;
	    $config['new_image'] = './ImageUpload/';
	    $config['maintain_ratio'] = false;
	    $settings['quality'] = '100';

	    $config['width'] = 400;
	    $config['height'] = 400;
	    $this->load->library('image_lib', $config);
	    if (!$this->image_lib->resize()) {
		$this->image_lib->display_errors();
	    }

	    $imagePath = "ImageUpload/" . preg_replace("/\s+/", "_", $newfilename);
	    $data = array('Image_Type_Id' => $ImageTypeId, 'Image_Title' => $ImageTitle, 'Image_Place' => $ImagePlace, 'Image_Content' => $ImageContent,
		'Upload_Image' => $imagePath, 'CreatedDate' => $createDate_time, 'IsActive' => 1);

	    $qry = $this->db->insert('tbl_imageupload', $data);

	    return "Ok";
	}
    }

    /* --------------------------Insert and Update Type Image Type Data----------------------------- */

    public function mgetImageTypeDetails() {
	return $result_set = $this->db->query("SELECT tit.Image_Type_Id, tit.Image_Type_Name, tit.ImageTypeContent FROM tbl_imagetype tit where tit.IsActive=1 ");
    }

    public function minsertImageTypeDetails() {
	$data = json_decode(file_get_contents("php://input"));
	$ImageType = ($data->ImageType);
	$ImageContent = ($data->ImageContent);

	$chk_qry = $this->db->query("SELECT tim.Image_Type_Id, tim.Image_Type_Name from tbl_imagetype tim WHERE tim.Image_Type_Name='" . $ImageType . "' AND tim.IsActive=1");
	if ($chk_qry->num_rows() > 0) {
	    return false;
	}


	$chk_qry = $this->db->query("SELECT tim.Image_Type_Id, tim.Image_Type_Name from tbl_imagetype tim WHERE tim.Image_Type_Name='" . $ImageType . "' AND tim.IsActive=0");
	if ($chk_qry->num_rows() > 0) {
	    $getData = $chk_qry->row();
	    $getImage_Type_Id = $getData->Image_Type_Id;

	    $data = array("ImageTypeContent" => $ImageContent, "IsActive" => 1);

	    $this->db->where("Image_Type_Id", $getImage_Type_Id);

	    $this->db->update("tbl_imagetype", $data);

	    return $this->db->query("SELECT (tit.Image_Type_Id) as Image_Type_Id FROM tbl_imagetype tit where Image_Type_Id='" . $getImage_Type_Id . "' and tit.IsActive=1 ");
	} else {
	    $data = array("Image_Type_Name" => $ImageType, "ImageTypeContent" => $ImageContent, "IsActive" => 1);
	    $this->db->insert("tbl_imagetype", $data);
	    return $this->db->query("SELECT max(tit.Image_Type_Id) as Image_Type_Id FROM tbl_imagetype tit where tit.IsActive=1 ");
	}
    }

    public function mupdateImageTypeDetails() {
	//$data			 =		json_decode($this->input->raw_input_stream , TRUE);

	$data = json_decode(file_get_contents("php://input"));

	$ImageTypeId = ($data->ImageTypeId);
	$ImageType = ($data->ImageType);
	$ImageContent = ($data->ImageContent);

	$chk_qry = $this->db->query("SELECT tim.Image_Type_Id, tim.Image_Type_Name from tbl_imagetype tim WHERE Image_Type_Id!='" . $ImageTypeId . "' and 
										  tim.Image_Type_Name='" . $ImageType . "' AND tim.IsActive=1");
	if ($chk_qry->num_rows() > 0) {
	    return false;
	} else {
	    $data = array("Image_Type_Name" => $ImageType, "ImageTypeContent" => $ImageContent, "IsActive" => 1);

	    $this->db->where("Image_Type_Id", $ImageTypeId);
	    return $this->db->update("tbl_imagetype", $data);
	}
    }

    public function mdeleteImageTypeDetails($Image_Type_Id) {
	$this->db->where("Image_Type_Id", $Image_Type_Id);
	return $this->db->update("tbl_imagetype", array("IsActive" => 0));
    }

}

//End class
?>