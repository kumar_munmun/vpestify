<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {
	public function __construct() {
		parent:: __construct();
        $this->load->database();
		$this->load->library('session');
    }
	
	public function mgetPestServices() {
		return $this->db->query("SELECT tps.Services_Id, tps.Service_Name, tps.Service_Content, tps.Service_Image FROM tbl_pestservices tps WHERE tps.IsActive=1");
	}
	
	public function mgetImageOnHome() {		
		$query	=	$this->db->query("SELECT GROUP_CONCAT(tit.Image_Type_Id) AS Image_Type_Id, tit.Image_Type_Name, tip.Image_Id, tip.Image_Title, 
									  GROUP_CONCAT(tip.Upload_Image) AS Upload_Image, tip.Image_Place, tip.Image_Content			
									  FROM tbl_imageupload tip									
									  INNER JOIN tbl_imagetype tit ON tit.Image_Type_Id=tip.Image_Type_Id AND tit.IsActive=1
									  WHERE tip.IsActive=1 AND tit.Image_Type_Name='Home' GROUP BY tit.Image_Type_Id,tip.Image_Place");
		return $query;
	}
	
	public function mgetImagetypeContent()	{
		return $this->db->query("SELECT tit.Image_Type_Id, tit.Image_Type_Name, tit.ImageTypeContent FROM tbl_imagetype tit WHERE tit.IsActive=1");
	}
	
} //End class
?>