<?php 
class Common_model extends CI_Model {
	function __construct()
	{
            parent::__construct();
	}
        
    public function get_count($table_name, $cond=array()){
        $this->db->where($cond);
        $this->db->from($table_name);
        return $this->db->count_all_results();
    }
        
}