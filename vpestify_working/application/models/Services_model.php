<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_Model
 {
	public function __construct()
	{
		parent:: __construct();
        $this->load->database();
		$this->load->library('session');
    }
	
	
	public function mgetAboutUs()
	{
		return $this->db->query("SELECT tmt.Image_Type_Id, tmt.Image_Type_Name, tmt.ImageTypeContent,
								GROUP_CONCAT(tip.Image_Id) AS Image_Id, GROUP_CONCAT(tip.Image_Title) AS Image_Title, GROUP_CONCAT(tip.Image_Place) AS Image_Place, 
								GROUP_CONCAT(tip.Upload_Image) AS Upload_Image, GROUP_CONCAT(tip.Image_Content) AS Image_Content

								FROM tbl_imagetype tmt
								LEFT JOIN tbl_imageupload tip ON tip.Image_Type_Id=tmt.Image_Type_Id AND tmt.IsActive=1
								WHERE tmt.Image_Type_Name='About'");
	}
	
	public function mgetPestServices()
	{
		$serviceName	=	$this->uri->segment(2);
		
		$service_Name	=	preg_replace("/\-/", " ",$serviceName);
		
		return $this->db->query("SELECT tps.Services_Id, tps.Service_Name, tps.Service_Content, tps.Service_Image
								 FROM tbl_pestservices tps WHERE tps.IsActive=1 AND tps.Service_Name='".$service_Name."'");
	}
	
	public function mgetPestServiceImageTop($serviceName = null) {
	$service_Name = preg_replace("/\-/", " ", $serviceName);
	$result = $this->db->query("SELECT tbl_imagetype.Image_Type_Name, tbl_imagetype.ImageTypeContent, tbl_imageupload.Image_Title, tbl_imageupload.Upload_Image, tbl_imageupload.Image_Place
				    FROM tbl_imagetype 
				    JOIN tbl_imageupload ON tbl_imageupload.Image_Type_Id = tbl_imagetype.Image_Type_Id
				    WHERE tbl_imagetype.Image_Type_Name = '". $service_Name ."' AND tbl_imageupload.Image_Place = 'top'");
	return $result;
    }
	
		
} //End class
?>