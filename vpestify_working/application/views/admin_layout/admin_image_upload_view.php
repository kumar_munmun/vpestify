<?php
      defined('BASEPATH') OR exit('No direct script access allowed');
	  
	  $this->load->helper('url');
?>
<?php include_once "shared/admin_header.php"; ?>

	<style>
		.none{display: none;}
		.glyphicon{font-size: 20px;}
		.glyphicon-plus{float: right;}
		a.glyphicon{text-decoration: none;cursor: pointer;}
		.glyphicon-trash{margin-left: 10px;}
		.alert{ width: 50%;	border-radius: 0; margin-top: 10px;	margin-left: 10px;	}
		.thumbnail{ height: 100px; width:180px; margin: 3px;}
		.cstmDiv{display:inline-block;float:left;}	
		.innerImage { height:50px; width:80px;}	
		#updateData { display:none;}
	</style>
	
	<div class="container">
		<div class="row">
	
	<div class="col-md-12" style='margin-top:70px'></div>
	
	<div class="col-md-12" style='margin-top:10px'>
		<div class="col-md-1 col-sm-1">	</div>
		
		 <div class="col-md-10 col-sm-10 panel panel-default users-content" style='margin-top:10px' >
            <div class="panel-heading">Add Image &nbsp; <a href="javascript:void(0);" class="glyphicon glyphicon-plus" onClick="$('.formData').slideToggle();"></a></div>
            <div class="alert alert-danger none"><p></p></div>
            <div class="alert alert-success none"><p></p></div>
			
            <div class="panel-body none formData">
								 
					<?php echo form_open_multipart('Admin/ImageUpload'); ?>
		
                    <div class="form-group col-md-3 col-sm-3">                      
                        <select class="form-control" name="ImageTypeId" id="ImageTypeId" required >
							<option value="">...Select Image Type...</option>
							<?php
								if(isset($imageType))
								{
									foreach($imageType as $rws)
									{
									?>
										<option value="<?php echo $rws->Image_Type_Id ?>"><?php echo $rws->Image_Type_Name ?></option>
									<?php
									}
								}
							?>
						</select>
                    </div>
                    <div class="form-group col-md-3 col-sm-3">                       
                        <input type="text" class="form-control" id="ImageTitle" name="ImageTitle" placeholder="Image Title" required />
                    </div>
					
					<div class="form-group col-md-3 col-sm-3">                       
                        <select class="form-control" name="ImagePlace" id="ImagePlace" required >
							<option value="">...Select image place...</option>
							<option value="top">Top</option>
							<option value="bottom">Bottom</option>
							<option value="right">Right</option>
							<option value="left">Left</option>
							<option value="middel">Middle</option>
							<option value="slider">Slider</option>
						</select>
                    </div>
					
					 <div class="form-group col-md-3 col-sm-3">                       
                         <input type="file" id="imageup_1" name="userfile" onChange="return GetFileSize()" class=" form-control"  />
                    </div>
					
					
					 <div class="form-group col-md-9 col-sm-9" id="ImageContentDiv">                       
                        <textarea class="form-control" name="ImageContent" id="ImageContent" ></textarea>
                    </div>
                     <div class="col-md-3 col-sm-3 ">
					 	
						<input type="hidden" class="form-control" name="Image_Id" id="Image_Id"  required />							
						 <a href="javascript:void(0);" class="btn btn-warning" onClick="$('.formData').slideUp();">Cancel</a>
						<input type="submit" name="addData" id="AddData" value="Submit" class="btn btn-success" />
						<input type="submit" name="updateData" id="updateData" value="Update" class="btn btn-primary" />
			 </form>			
						<div class="col-md-12 col-sm-12">
							<output id="result" />
							<span id="selected_image" ></span>
						</div>
						
					</div>               		
			</div>
	</div>
		
		<div class="col-md-1 col-sm-1">	</div>
		
	</div>
	
	<div class="col-md-12 col-sm-12" style='margin-top:10px'>
	
		<!--<div class="col-md-1 col-sm-1">	</div>-->
		
		<div class="col-md-12 col-sm-12"> 
			<table class="table table-responsive table-hover table-striped table-bordered">
				<tr><th>Sn.</th><th>ImageType</th><th>Title</th><th>ImagePlace</th><th>Content</th><th>Image</th><th>Action</th></tr>				
				<?php
					if(isset($imageUpload))
					{
						$i=1;
						foreach($imageUpload as $rws)
						{
							echo "<tr>";
								echo "<td>".$i++."</td>";
								echo "<td id='imageType".$rws->Image_Id."'>".$rws->Image_Type_Name."</td>";
								echo "<td id='imageTitle".$rws->Image_Id."'>".$rws->Image_Title."</td>";
								echo "<td id='imagePlace".$rws->Image_Id."'>".$rws->Image_Place."</td>";
								echo "<td id='imageContent".$rws->Image_Id."'>".$rws->Image_Content."</td>";
								echo "<td id='imageSrc".$rws->Image_Id."'><img class='innerImage' src='".base_url().$rws->Upload_Image."' /></td>";
								echo "<td><a href='javascript:void(0);' class='btn btn-primary' onclick='editForm(".$rws->Image_Id.", ".$rws->Image_Type_Id.")'>Edit</a>
									<a href='".base_url()."index.php/Admin/deleteImageUpload/".$rws->Image_Id."' class='btn btn-danger del_confirm'>Dalete</a></td>";
							echo "</tr>";
						}
					}
				?>
				
			</table>
		</div>		
		<!--<div class="col-md-1 col-sm-1">	</div>		-->
	</div>
	
	<div class="col-md-12" style='margin-top:70px'></div>
	
	</div>
	</div>
	
	<script src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
	<script>
	 tinymce.init({
	  selector: '#ImageContent',
	  height: 'auto',
	  theme: 'modern',
	  plugins: [
		'advlist autolink lists charmap print preview hr anchor pagebreak',
		'searchreplace wordcount visualblocks visualchars code fullscreen',
		'insertdatetime nonbreaking contextmenu directionality',
		'emoticons template paste textcolor colorpicker textpattern'
	  ],
	  toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
	  image_advtab: true,
	  templates: [
		{ title: 'Test template 1', content: 'Test 1' },
		{ title: 'Test template 2', content: 'Test 2' }
	  ]
	 });
	 
	//-----------Fet Image-------------------
	
    function GetFileSize() {
	
		   $('#selected_image').text("");
			$('#result').text("");
			
        var filesInput = document.getElementById('imageup_1');
       
		var files = filesInput.files; //FileList object
            var output = document.getElementById("result");
            var name = "image";
            for(var i = 0; i<= filesInput.files.length-1; i++)
            {
                var file = files[i];
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener("load",function(event){
                    
                    var picFile = event.target;
                    document.getElementById('result').innerHTML = document.getElementById('result').innerHTML+ "<img class='thumbnail cstmDiv' src='" + picFile.result + "'" + "title='" + "image" + "'/>";        
                });
                //Read the image
                picReader.readAsDataURL(file);
				//alert(picReader);
            }               
    }
	
	function editForm(Image_Id, Image_Type_Id)
	{
		
		$("#AddData").hide();
		$("#updateData").show();
		
		
		$('.formData').slideDown();
		
		$('#Image_Id').val(Image_Id);
		
		var imageTitle	=	$("#imageTitle"+Image_Id).text();
		$("#ImageTitle").val(imageTitle);
		
		var imageContent	=	$("#imageContent"+Image_Id).text();
		$("#ImageContentDiv").html('<textarea class="form-control" name="ImageContent" id="ImageContent" >'+imageContent+'</textarea>');
		
		var ImageTypeIdarr 	=	document.getElementById('ImageTypeId');	
		for(var i=1; i<ImageTypeIdarr.length; i++ )
		{
			if(Image_Type_Id==ImageTypeIdarr.options[i].value)
			{
				ImageTypeIdarr.options[i].selected=true;
			}
		}
		
		var ImagePlacearr 		=	document.getElementById('ImagePlace');
		var imagePlace			=	$("#imagePlace"+Image_Id).text();
		//alert(imagePlace);
		for(var i=1; i<ImagePlacearr.length; i++ )
		{
			if(imagePlace==ImagePlacearr.options[i].value)
			{
				ImagePlacearr.options[i].selected=true;
			}
		}
		
	}
	
	/*--------------------Delete Data---Confirmation------------------------*/
	 $(document).ready(function(){
		$(".del_confirm").click(function(){
			var $del	=	confirm("Are you sure want to delete this product");
			if($del==false)
			{
				return false;
			}
		});
	 });
</script>
      <!-- FOOTER -->	  
 <?php include_once "shared/footer.php"; ?>

    