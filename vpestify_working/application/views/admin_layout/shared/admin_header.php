<?php
	  $this->load->helper('url');
?>
<html>
<head>

	<title>Admin Panel</title>
	
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom_css.css" type="text/css">
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css" type="text/css">
	<!-- added css-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" type="text/css">
	
	<!-- up to here added -->
	<link href="<?php echo base_url(); ?>assets/css/yamm.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js" type="text/javascript"></script>
	
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/moment.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>

	<script src="<?php echo base_url(); ?>assets/js/angular.min_1.5.7.js" type="text/javascript"></script>
	
	<!--<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>-->
	
	<script>
      $(function() {
        $(document).on('click', '.yamm .dropdown-menu', function(e) {
          e.stopPropagation()
        })
      })
    </script>	
	
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url()?>assets/css/jquery-ui.css" />
	
	
</head>
<body>
	<header>
		<div class="nav yamm navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>					
					
					<img src="" class="img-responsive" style="width: 100px; height:50px" />
						
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">						
						
						  <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle fee_report_lnk">ImgeType<b class="caret"></b></a>
							<ul class="dropdown-menu">
							  <li>
								<div class="yamm-content">
								  <ul class="media-list">
								  	<li class="media"><a href="<?php echo site_url("Admin/ImageType")?>" class="pull-left">Image type</a></li>
									<!--<li class="media"><a href="<?php //echo site_url("Studentfeereport/studentFeeReport/paid")?>" class="pull-left">Fee Paid</a></li>-->																
									<li class="media"><a href="<?php echo site_url("Admin/ImageUpload")?>" class="pull-left">Image Upload</a></li>
								
								  </ul>
								</div>
							  </li>
							</ul>
						  </li>
						  
						  <li><a href="<?php echo site_url("Admin/insertServices")?>" class="">Services</a></li>
						  
						   <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle email_lnk">Email<b class="caret"></b></a>
							<ul class="dropdown-menu">
							  <li>
								<div class="yamm-content">
								  <ul class="media-list">
									<li class="media"><a href="<?php echo site_url("Studentnotification/Allstudentemail_notification/total")?>" class="pull-left">
														Total Enquiry</a>
									</li>
									<li class="media"><a href="<?php echo site_url("Studentnotification/Registeredstudentemail_notification/registered")?>" class="pull-left">
														Registered Students</a>
									</li>
									<li class="media"><a href="<?php echo site_url("Studentnotification/Pendingstudentemail_notification/pending")?>" class="pull-left">
														Pending Admmision</a>
									</li>
									<li class="media"><a href="<?php echo site_url("Studentnotification/Demostudentemail_notification/demo")?>" class="pull-left">
														Demo Students</a>
													
									</li>
								  </ul>
								</div>
							  </li>
							</ul>
						  </li>
					</ul>
					
					<ul class="nav navbar-nav navbar-right">
					  <!-- Forms -->
					  <li class="dropdown">
					  	<?php 
							if(isset($_SESSION['admin']))
							{
								echo '<a href="'.site_url('Logout/logout').'"> Logout </a>';
							}
							else {
									echo '<a href="#" data-toggle="dropdown" class="dropdown-toggle">Login<b class="caret"></b></a>';
							 }	
						?>						
						
						<ul class="dropdown-menu">
						  <li>
							<div class="yamm-content">
							 <?php $this->load->helper('form');
			 						 echo form_open('Admin/LoginAuthentic',  array('id'=>'login'));
			 				 ?>
									<div class="form-group">
									  <input id="inputName" name="user" type="text" placeholder="Name" class="form-control">
									</div>
									<div class="form-group">
									  <input id="inputPassword" name="password" type="password" placeholder="Password" class="form-control">
									</div>									
									<div class="form-group">
									  <button type="submit" name="btn_submit" class="btn btn-success">Submit</button>
									</div>								
							 		<?php echo form_close();?>
							</div>
						  </li>
						</ul>
					  </li>
					</ul>
				</div>
			</div>
		</div>
	</header>
	
	