<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->helper('url');
?>
<?php include_once "shared/admin_header.php"; ?>

<style>
    .none{display: none;}
    .glyphicon{font-size: 20px;}
    .glyphicon-plus{float: right;}
    a.glyphicon{text-decoration: none;cursor: pointer;}
    .glyphicon-trash{margin-left: 10px;}
    .alert{ width: 50%;	border-radius: 0; margin-top: 10px;	margin-left: 10px;	}
    .thumbnail{ height: 100px; width:180px; margin: 3px;}
    .cstmDiv{display:inline-block;float:left;}	
    .innerImage { height:50px; width:80px;}	
    #updateData { display:none;}
</style>

<div class="container">
    <div class="row">

	<div class="col-md-12" style='margin-top:70px'></div>

	<div class="col-md-12" style='margin-top:10px'>
	    <div class="col-md-1 col-sm-1">	</div>

	    <div class="col-md-10 col-sm-10 panel panel-default users-content" style='margin-top:10px' >
		<div class="panel-heading">Add Image &nbsp; <a href="javascript:void(0);" class="glyphicon glyphicon-plus" onClick="$(
				'.formData').slideToggle();"></a></div>

		<?php if ($this->session->flashdata('err_msg')) { ?> <div class="alert alert-danger "><p><?php
		    echo $this->session->flashdata('err_msg');
		    echo '</p></div>';
		}
		?>
			<?php if ($this->session->flashdata('msg')) { ?> <div class="alert alert-success "><p><?php
			    echo $this->session->flashdata('msg');
			    echo '</p></div>';
			}
			?>

			<div class="panel-body none formData">

			    <?php echo form_open_multipart('Admin/insertServices'); ?>

			    <div class="form-group col-md-3 col-sm-3">                       
				<input type="text" class="form-control" id="ServiceName" name="ServiceName" placeholder="Service Name" required />
			    </div>

			    <div class="form-group col-md-9 col-sm-9" id="ServiceContentDiv">                       
				<textarea class="form-control" name="ServiceContent" id="ServiceContent"></textarea>
			    </div>

			    <div class="form-group col-md-3 col-sm-3">                       
				<input type="file" id="imageup_1" name="userfile" onChange="return GetFileSize()" class=" form-control"  />
			    </div>

			    <div class="col-md-3 col-sm-3 ">

				<input type="hidden" class="form-control" name="Services_Id" id="Services_Id"  required />

				<a href="javascript:void(0);" class="btn btn-warning" onClick="$(
						'.formData').slideUp();">Cancel</a>
				<input type="submit" name="addData" id="AddData" value="Submit" class="btn btn-success" />
				<input type="submit" name="updateData" id="updateData" value="Update" class="btn btn-primary" />
				</form>	

			    </div>  					

			    <div class="col-md-6 col-sm-6" style="border:1px solid #CCC">
				<output id="result" />
				<span id="selected_image" ></span>
			    </div>

			</div>
		    </div>

		    <div class="col-md-1 col-sm-1"></div>

		</div>

		<div class="col-md-12 col-sm-12" style='margin-top:10px'>

		    <!--<div class="col-md-1 col-sm-1">	</div>-->

		    <div class="col-md-12 col-sm-12"> 
			<table class="table table-responsive table-hover table-striped table-bordered">
			    <tr><th>Sn.</th><th>Service_Name</th><th>Service_Content</th><th>Image</th><th>Action</th></tr>				
			    <?php
			    if (isset($services)) {
				$i = 1;
				foreach ($services as $rws) {
				    echo "<tr>";
				    echo "<td>" . $i++ . "</td>";
				    echo "<td id='serviceName" . $rws->Services_Id . "'>" . $rws->Service_Name . "</td>";
				    echo "<td id='serviceContent" . $rws->Services_Id . "'>" . $rws->Service_Content . "</td>";
				    echo "<td id='imageSrc" . $rws->Services_Id . "'><img class='innerImage' src='" . base_url() . $rws->Service_Image . "' /></td>";

				    echo "<td><a href='javascript:void(0);' class='btn btn-primary' onclick='editForm(" . $rws->Services_Id . ")'>Edit</a>
									<a href='" . base_url() . "index.php/Admin/deleteServiceData/" . $rws->Services_Id . "' class='btn btn-danger del_confirm'>Delete</a></td>";
				    echo "</tr>";
				}
			    }
			    ?>

			</table>
		    </div>		
		    <!--<div class="col-md-1 col-sm-1">	</div>		-->
		</div>

		<div class="col-md-12" style='margin-top:70px'></div>

	    </div>
	</div>

	<script src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
	<script>
	    //-----------Fet Image-------------------
	    function GetFileSize() {
		$('#selected_image').text("");
		$('#result').text("");

		var filesInput = document.getElementById('imageup_1');
		var files = filesInput.files; //FileList object
		var output = document.getElementById("result");
		var name = "image";
		for (var i = 0; i <= filesInput.files.length - 1; i++){
		    var file = files[i];
		    //Only pics
		    if (!file.type.match('image'))
			continue;

		    var picReader = new FileReader();
		    picReader.addEventListener("load", function (event) {
				var picFile = event.target;
				document.getElementById(
					'result').innerHTML = document.getElementById(
					'result').innerHTML + "<img class='thumbnail cstmDiv' src='" + picFile.result + "'" + "title='" + "image" + "'/>";
			    });
		    //Read the image
		    picReader.readAsDataURL(file);
		    //alert(picReader);
		}
	    }

	    function editForm(Services_Id)
	    {
		$("#AddData").hide();
		$("#updateData").show();
		$('.formData').slideDown();
		$('#Services_Id').val(Services_Id);
		var serviceName = $("#serviceName" + Services_Id).text();
		$("#ServiceName").val(serviceName);

		//var serviceContent = $("#serviceContent" + Services_Id).text();
		//$("#ServiceContentDiv").html('<textarea class="form-control" name="ServiceContent" id="ServiceContent" >' + serviceContent + '</textarea>');
		//$("#ServiceContent").text(serviceContent);
	    }

	    /*--------------------Delete Data---Confirmation------------------------*/
	    $( document).ready(function () {
		$(".del_confirm").click(function () {
			var $del = confirm("Are you sure want to delete this product");
			if ($del == false){
			    return false;
			}
		});
	   });

	    tinymce.init({
			selector: '#ServiceContent',
			height: 'auto',
			theme: 'modern',
			plugins: [
			    'advlist autolink lists charmap print preview hr anchor pagebreak',
			    'searchreplace wordcount visualblocks visualchars code fullscreen',
			    'insertdatetime nonbreaking contextmenu directionality',
			    'emoticons template paste textcolor colorpicker textpattern'
			],
			toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
			image_advtab: true,
			templates: [
			    {title: 'Test template 1', content: 'Test 1'},
			    {title: 'Test template 2', content: 'Test 2'}
			]
		    });

	</script>
	<!-- FOOTER -->	  
	<?php include_once "shared/footer.php"; ?>

