<?php
      defined('BASEPATH') OR exit('No direct script access allowed');
	  
	  $this->load->helper('url');
?>
<?php include_once "shared/admin_header.php"; ?>

<div class="container">
		<div class="row">
	
	
	<div class="col-md-12" style='margin-top:70px'></div>
	<div ng-controller="imageTypeController" ng-init="getRecords()" ng-app="imageTypeModule">
	
	 <div class="col-md-12 col-sm-12 panel panel-default users-content" style='margin-top:10px' >
            <div class="panel-heading">Add ImageType <a href="javascript:void(0);" class="glyphicon glyphicon-plus" onClick="$('.formData').slideToggle();"></a></div>
            <div class="alert alert-danger none"><p></p></div>
            <div class="alert alert-success none"><p></p></div>
			
            <div class="panel-body none formData">
				 <form class="form" name="userForm">
				 
					 <input type="hidden" class="form-control" placeholder="Image Type" name="Image_Type_Id" ng-model="tempImageTypeRecords.Image_Type_Id"/>
				 
                    <div class="form-group col-md-4 col-sm-4">                      
                        <input type="text" class="form-control" placeholder="Image Type" name="ImageType" ng-model="tempImageTypeRecords.ImageType"/>
                    </div>
                    <div class="form-group col-md-5 col-sm-5">  
					  <textarea id="ImageContent" class="form-control" name="ImageContent" placeholder="Image Content" ng-model="tempImageTypeRecords.ImageContent" ></textarea>                    
                       <!-- <input type="text" class="form-control" name="ImageContent" placeholder="Image Content" ng-model="tempImageTypeRecords.ImageContent"/>-->
                    </div>
                     <div class="col-md-3 col-sm-3 ">
                   		 <a href="javascript:void(0);" class="btn btn-warning" onClick="$('.formData').slideUp();">Cancel</a>
                   		 <a href="javascript:void(0);" class="btn btn-success" ng-hide="tempImageTypeRecords.Image_Type_Id" ng-click="insertData()">Add User</a>
                   		 <a href="javascript:void(0);" class="btn btn-success" ng-hide="!tempImageTypeRecords.Image_Type_Id" ng-click="updateData()">Update User</a>
					</div>
                </form>				
			</div>
	</div>
	
	<div class="col-md-12 col-sm-12" style='margin-top:10px'>
		<div class="col-md-2 col-sm-2">
		</div>
		
		<div class="col-md-8 col-sm-8">
			<input type="text" class="form-control" placeholder="Serach" ng-model="searchItem" />
		</div>
		
		<div class="col-md-2 col-sm-2">
		</div>
	</div>
	
	<div class="col-md-12 col-sm-12" style='margin-top:10px'>
			
		<div class="col-md-1 col-sm-1">
		</div>
		
		<div class="col-md-10 col-sm-10" >
			<table class="table table-responsive table-hover table-striped table-bordered">
				<tr><th>Sn.</th><!--<th>typeId</th>--><th ng-click="sortBy('Image_Type_Name')" >ImageType</th>
					<th ng-click="sortBy('ImageTypeContent')" >ImageTypeContent</th><th style="text-align:center">Action</th></tr>	
				<tr ng-repeat="ImageType in ImageTypeRecords | filter:searchItem | orderBy:sortField:reverseOrder">
					<td>{{$index+1}}</td>
					<!--<td>{{ImageType.Image_Type_Id}}</td>-->
					<td>{{ImageType.Image_Type_Name}}</td>
					<td>{{ImageType.ImageTypeContent}}</td>
					 <td style="text-align:center">
                        <a href="javascript:void(0);" class="btn btn-success glyphicon glyphicon-edit" ng-click="editData(ImageType)"></a>
                        <a href="javascript:void(0);" class="btn btn-danger glyphicon glyphicon-trash" ng-click="deleteData(ImageType)"></a>
                    </td>
				</tr>			
			</table>
			
			<div><dir-pagination-control> </dir-pagination-control></div>
		</div>
		
		<div class="col-md-1 col-sm-1">
			
			 <div >
				  {{total_records}}
			 </div>
		</div>
		
		<div class="col-md-12" style='margin-top:70px; height:100px'></div>
		
	</div>
	
	</div>
		
	<script>
		angular.module('imageTypeModule',[])
				.controller("imageTypeController", function($scope, $http)
		{	
			 $scope.tempImageTypeRecords = {};  
			 $scope.ImageTypeRecords = [];			
			   		
   			 $scope.getRecords = function(){
					$http.get('GetImageTypeDetails', {
						params:{
							'type':'view'
						}
					}).success(function(response){
							$scope.ImageTypeRecords = response.records;
							$scope.total_records	= response.records.length;	
				});
			};
			
				$scope.sortcolumn = "Image_Type_Name";
					
					$scope.reverseOrder = true;
					$scope.sortField = 'Image_Type_Name';
					$scope.sortBy = function(sortField) {					
					  $scope.reverseOrder = ($scope.sortField === sortField) ? !$scope.reverseOrder : false;					
					  $scope.sortField = sortField;				
					};					
			
			// Edit Data Display in text field
			
			 $scope.editData = function(getEditData){
				
				$scope.tempImageTypeRecords = {
							Image_Type_Id:getEditData.Image_Type_Id,
							ImageType:getEditData.Image_Type_Name,
							ImageContent:getEditData.ImageTypeContent,
				};
				$scope.index = $scope.ImageTypeRecords.indexOf(getEditData);				
				$('.formData').slideDown();
			};
			
			// Function to insert data
			
			$scope.insertData=function(){    
				 $http({
						method:'post',
						url:'insertImageType',
						data : {ImageType:$scope.tempImageTypeRecords.ImageType,ImageContent:$scope.tempImageTypeRecords.ImageContent }, //forms user object
						headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
					}).success(function (data)
						{						
							if(data.chk_status=="OK")
							{
								//alert(data.Image_Type_Id);
								
								$scope.messageSuccess("Data inserted");
							    $scope.ImageTypeRecords.push({
									Image_Type_Id:data.Image_Type_Id,
									Image_Type_Name:$scope.tempImageTypeRecords.ImageType,
									ImageTypeContent:$scope.tempImageTypeRecords.ImageContent,
								});
								 $('.formData').slideUp();

							}else { $scope.messageError(data.chk_status); }
						});
	
					/*$http.post("insertImageType", {
								'ImageType':$scope.ImageType,
								'ImageContent':$scope.ImageContent
					}).success(function(response){
							console.log("Data Inserted Successfully");
						},function(error){
							alert("Sorry! Data Couldn't be inserted!");
							console.error(error);
					 });*/
            };
			
			 // function to delete user data from the database
				$scope.deleteData = function(Image_Type_Id){
				
					var conf = confirm('Are you sure to delete the user?');
					if(conf === true){
						var data = $.param({
							'Image_Type_Id': Image_Type_Id.Image_Type_Id,
							'type':'delete' 
						});
						var config = {
							headers : {
								'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
							}    
						};
						$http.post("deleteImageType",data,config).success(function(response){
							if(response.chk_status == 'OK'){
								var index = $scope.ImageTypeRecords.indexOf(Image_Type_Id);
								
								$scope.ImageTypeRecords.splice(index,1);
								$scope.messageSuccess("Data Deleted ");
							}else{
								$scope.messageError("Error");
							}
						});
					}
				};
				
				
			// Function to Update data------------------
			
			$scope.updateData=function(){    
				 $http({
						method:'post',
						url:'UpdateImageType',
						data : {ImageTypeId:$scope.tempImageTypeRecords.Image_Type_Id,ImageType:$scope.tempImageTypeRecords.ImageType,ImageContent:$scope.tempImageTypeRecords.ImageContent }, //forms user object
						headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
					}).success(function (data)
						{						
							if(data.chk_status=="OK")
							{
								 	$scope.ImageTypeRecords[$scope.index].Image_Type_Id		 =	 $scope.tempImageTypeRecords.Image_Type_Id;
									$scope.ImageTypeRecords[$scope.index].Image_Type_Name	 =	 $scope.tempImageTypeRecords.ImageType;
									$scope.ImageTypeRecords[$scope.index].ImageTypeContent	 =	 $scope.tempImageTypeRecords.ImageContent;
												
								$scope.messageSuccess("Data Updated");
							}else { $scope.messageError(data.chk_status); }
						});
            };
			
			
			// Display Message-------------------
			
		   $scope.messageSuccess = function(msg){
		   		
				$('.alert-success > p').html(msg);
				$('.alert-success').show();
				$('.alert-success').delay(3000).slideUp(function(){
					$('.alert-success > p').html('');
				});
			};
			
			// function to display error message
			$scope.messageError = function(msg){
				$('.alert-danger > p').html(msg);
				$('.alert-danger').show();
				$('.alert-danger').delay(3000).slideUp(function(){
					$('.alert-danger > p').html('');
				});
			};
	
		});
	</script>
	
	<style>
		.none{display: none;}
		.glyphicon{font-size: 20px;}
		.glyphicon-plus{float: right;}
		a.glyphicon{text-decoration: none;cursor: pointer;}
		.glyphicon-trash{margin-left: 10px;}
		.alert{ width: 50%;	border-radius: 0; margin-top: 10px;	margin-left: 10px;	}
	</style>
      <!-- FOOTER -->	  
 <?php include_once "shared/footer.php"; ?>

    