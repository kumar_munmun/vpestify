<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/plugins/simple-line-icons/simple-line-icons.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/plugins/uniform/css/uniform.default.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/backend/plugins/bootstrap-select/css/bootstrap-select.min.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/plugins/select2/select2.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/backend/plugins/jquery-multi-select/css/multi-select.css');?>" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');?>"/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/plugins/bootstrap-datepicker/css/datepicker3.css');?>"/>

<!-- END GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url('assets/backend/pages/css/login.css');?>" rel="stylesheet" type="text/css"/>

<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url('assets/backend/css/components.css');?>" id="style_components" rel="stylesheet" type="text/css"/>


<link href="<?php echo base_url('assets/backend/css/plugins.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/layout/css/layout.css');?>" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?php echo base_url('assets/backend/layout/css/themes/default.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/layout/css/custom.css');?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/backend/mycss/mycss.css');?>" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<script src="<?php echo base_url('assets/backend/plugins/jquery.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/jquery-migrate.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/js/jqajax.js?q='.VERSION)?>"></script>
<script src="<?php echo base_url('assets/backend/js/common.js?q='.VERSION)?>"></script>
<script src="<?php echo base_url('assets/backend/js/jqcommon.js?q='.VERSION)?>"></script>
<script src="<?php echo base_url('assets/backend/pages/scripts/custom.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/pages/scripts/components-pickers.js')?>"></script>
<script src="<?php echo base_url('assets/backend/js/validation.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php //echo base_url('assets/backend/js/bootstrap-datepicker.js')?>"></script>
<script type="text/javascript">var SITE_URL='<?php echo base_url();?>';</script>

<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
