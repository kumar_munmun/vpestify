<?php $page_limit= $this->input->get('page_limit'); ?>
<?php echo form_open(isset($form_action)?$form_action:'',array('method'=>'get','id'=>'list_shown_form'));?>
    <div class="input-group pull-right">
        <?php  
            $page_opt=array(
                '10'    =>  10,
                '20'    =>  20,
                '50'    =>  50,
                '100'   =>  100
                );
            echo form_dropdown('page_limit',$page_opt,!empty($page_limit)?$page_limit:10,'class="form-control list_shown"');
        ?>                
    </div>
    <div class="clearfix"></div>
<?php echo form_close() ?>