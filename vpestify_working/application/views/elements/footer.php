
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url('assets/backend/plugins/respond.min.js');?>"></script>
<script src="<?php echo base_url('assets/backend/js/excanvas.min.js');?>"></script> 
<![endif]-->

<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url('assets/backend/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/jquery.blockui.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/jquery.cokie.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/uniform/jquery.uniform.min.js');?>" type="text/javascript"></script>
<!--<script src="<?php echo base_url('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');?>" type="text/javascript"></script>-->
<script src="<?php echo base_url('assets/backend/js/add_more_file.js');?>"></script> 
<!-- END CORE PLUGINS -->

<script src="<?php echo base_url('assets/backend/plugins/jquery-validation/js/jquery.validate.min.js');?>" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url('assets/backend/plugins/bootstrap-select/bootstrap-select.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/select2/select2.min.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/plugins/jquery-multi-select/js/jquery.multi-select.js');?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/backend/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/backend/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>

<script src="<?php echo base_url('assets/backend/scripts/metronic.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/layout/scripts/layout.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/layout/scripts/quick-sidebar.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend/layout/scripts/demo.js');?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/backend/js/main.js?q='.VERSION)?>"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url('assets/backend/js/custom.js')?>"></script>