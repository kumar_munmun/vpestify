<?php $PicsUser=$_SESSION['PicsUser'];
        
if(!empty($_SESSION['PicsUser']['image']))
{
    $img = base_url(PROFILE_IMG_DIR.$_SESSION['PicsUser']['image']);
}else
{
    $img = AURL."layout/img/avatar3_small.jpg";
}
?>
<div class="page-header navbar navbar-fixed-top">
<div class="page-header-inner">
    <div class="page-logo">
        <a href="<?php echo URL; ?>">
            <img src="<?php echo AURL; ?>img/logo.png" alt="logo" class="logo-default"  height="50px;" width="50px;" style="margin-top:-4px"/>
        </a>
        <div class="menu-toggler sidebar-toggler hide">
            <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
        </div>
    </div>

    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>

    <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown dropdown-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img alt="" class="img-circle" src="<?php echo $img; ?>"/>
                    <span class="username username-hide-on-mobile"><?php echo $loggedData['userName']; ?></span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                    <li><?php echo anchor("logout",'<i class="fa fa-sign-out"></i>Log Out'); ?></li>
                    <li><a href="<?php echo URL . "user/update_profile/" . encode($PicsUser['id']); ?>""><i class="fa fa-sign-out"></i>Update Profile</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
</div>
<div class="clearfix"></div>