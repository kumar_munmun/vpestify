<?php

/**
* View for sidebar
* @ModifiedAT: 27 Oct 2015
* @ModifiedBY: Atendra
* @Email:      atendra@itglobalconsulting.com
*/

$first_part = $this->uri->segment(1);
$second_part="/".$this->uri->segment(2);
$perms=$loggedData['perms'];

//echo $first_part exit;
//pr($perms); die();

$sidebar_arr=get_sidebar(); //pr($sidebar_arr);  die();

$sidebar_name = get_sidebar_name(); //pr($sidebar_name); die();

$sidebar = get_sidebar_name_array();

//pr($this->session->all_userdata());die;

?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
    	<!-- Main Sidebar -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper"><div class="sidebar-toggler"></div></li>
            <!-- Dashboard -->
            <li class="start <?php echo (!empty($first_part) && ($first_part=="dashboard"))?"active":""; ?>">
                <?php echo anchor('dashboard','<i class="fa fa-home"></i><span class="title">Dashboard</span>',array()) ?>
            </li>
            <?php if($this->session->userdata['PicsUser']['roleId']==1): ?>
            <!-- Manage Permissions Name -->
            <li class="<?php if($first_part=="manage_permissions" || $first_part=="add_permission" || $first_part=="edit_permission" || $first_part=="view_permission" ){echo "open active";}else {echo "";} ?>" Style="display:none;">
                <a href="javascript:;">
                    <i class="fa fa-cogs"></i><span class="title">Manage Permissions</span><span class="arrow"></span>
                </a>                
                <ul class="sub-menu" id="<?php if ($first_part=="b_user_list" || $first_part=="b_add_user" || $first_part=="view_business_user") {echo "navopenblock"; } else  {echo "";}?>">                    
                    <li class="<?php echo ($first_part=="manage_permissions" || $first_part=="edit_permission")?"active":""; ?>">
                        <?php echo anchor('manage_permissions','<i class="fa fa-caret-right"></i>View List'); ?>
                    </li>
                     <li class="<?php echo ($first_part=="add_permission")?"active":""; ?>" >
                        <?php echo anchor('add_permission','<i class="fa fa-caret-right"></i>Add New'); ?>
                    </li>
                </ul>
            </li>
            
            <!-- End Manage Permissions Name -->
            <?php endif; ?>
            
            <?php foreach($sidebar_arr as $key=>$val): ?>
                <?php if(!empty($perms[$val['id']]['parentNav']) || !empty($perms[$val['id']]['parentNav'])):?>
                <?php   
                    $link       = (!empty($val['permissionController']))?$val['permissionController']:'#';
                    $add_link   = (!empty($link) && $link!='#')?'add_'.$link:''; 
                    $view_link  = (!empty($link) && $link!='#')?'view_'.$link:'';
                    $edit_link  = (!empty($link) && $link!='#')?'edit_'.$link:'';
                    $save_link  = (!empty($link) && $link!='#')?'save_'.$link:'';
                    $assign_link  = (!empty($link) && $link!='#')?'assing-verifier-list': '';
                    
                    $report_link = array('list-assinged-report', 'assing-verifier-list');
                ?>
                <li class="<?php echo ($first_part==$link || $first_part==$add_link || $first_part==$edit_link || $first_part==$view_link || $first_part==$save_link || (($link == 'report') && in_array($first_part, $report_link))) ? "open active" : "";?>">
                    <a href="javascript:;">
                        <?php echo stripslashes($val['permissionIcon']);  ?>
                      
                      <!--<span class="title"><?php echo $val['permissionName']; ?></span>-->
                       <span class="title"><?php if($val['id']==1){echo ucwords($sidebar[0]['permissionName']);}
                       elseif($val['id']==2){
                          echo ucwords($sidebar[1]['permissionName']);
                       }
                       elseif($val['id']==3){
                           echo ucwords($sidebar[2]['permissionName']);
                       }
                        elseif($val['id']==4){
                          echo ucwords($sidebar[3]['permissionName']);
                       }
                        elseif($val['id']==5){
                         echo ucwords($sidebar[4]['permissionName']);
                       }
                        elseif($val['id']==6){
                          echo ucwords($sidebar[5]['permissionName']);
                       }
                        elseif($val['id']==7){
                          echo ucwords($sidebar[6]['permissionName']);
                       }
                        elseif($val['id']==8){
                          echo ucwords($sidebar[7]['permissionName']);
                       }
                        elseif($val['id']==9){
                          echo ucwords($sidebar[8]['permissionName']);
                       }
                        elseif($val['id']==10){
                           echo ucwords($sidebar[9]['permissionName']);
                       }
                        elseif($val['id']==11){
                         echo ucwords($sidebar[10]['permissionName']);
                       }
                        elseif($val['id']==12){
                           
                           echo ucwords($sidebar[11]['permissionName']);
                       }
                       elseif($val['id']==13){
                            
                           echo ucwords($sidebar[12]['permissionName']);
                       }
                       elseif($val['id']==14){
                           echo ucwords($sidebar[13]['permissionName']);
                       }
                       elseif($val['id']==15){
                           echo ucwords($sidebar[14]['permissionName']);
                       }
                       elseif($val['id']==16){
                           echo ucwords($sidebar[15]['permissionName']);
                       }
                       
                       
                       ?></span>
                        <!--<span class="title"><?php //if($key==1) echo $sidebar_name['Manage_Roles']; ?></span>-->
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" id="<?php echo ($first_part==$link || $first_part==$add_link || $first_part==$edit_link || $first_part==$view_link || $first_part==$save_link)?"navopenblock":'';?>">                        
                        <?php if(!empty($perms[$val['id']]['viewNav'])): ?>
                        <li class="<?php echo ($first_part==$link || $first_part==$edit_link || $first_part==$view_link)?"active":""; ?>">
                            <?php echo anchor($link,'<i class="fa fa-caret-right"></i>View List');  ?>
                        </li>
                        <?php  endif; ?>
                        
                        <?php if(!empty($perms[$val['id']]['addNav'])): ?>
                        <li class="<?php echo ($first_part==$add_link)?"active":""; ?>" <?php if($link=='report' || $link=='role' ) echo "Style=display:none;" ?>>
                            <?php echo anchor($add_link,'<i class="fa fa-caret-right"></i>Add New'); 
                            ?>
                        </li>
                        <?php endif; ?>
                        
                        <?php if(!empty($perms[$val['id']]['assignNav'])): ?>
                        <li class="<?php echo ($first_part==$assign_link)?"active":""; ?>">
                            <?php echo anchor($assign_link,'<i class="fa fa-caret-right"></i>Assign Verifier List'); 
                            ?>
                        </li>
                        <?php endif; ?>
                                              
                        
                        <?php if($link == 'report'): ?>
                        <!--li class="<?php echo ($first_part=='assing-verifier-list')?"active":""; ?>">
                            <?php //echo anchor('assing-verifier-list','<i class="fa fa-caret-right"></i>Assign Verifier List');?>
                        </li-->
                        
                        <?php if($this->session->userdata('PicsUser')['roleId'] == 14) { ?>
                        <li class="<?php echo ($first_part=='list-assinged-report')?"active":""; ?>">
                            <?php echo anchor('list-assinged-report','<i class="fa fa-caret-right"></i>Assinged Report List'); 
                            ?>
                        </li>
                        <?php } ?>
                        
                        <?php endif; ?>
                        
                    </ul>                    
                </li>
                <?php endif; ?>  
            <?php  endforeach; //die(); ?>                           
        </ul>
    </div>
</div>
<?php //print_r($sidebar_name);die;?>