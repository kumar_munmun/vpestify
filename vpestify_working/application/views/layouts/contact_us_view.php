<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?>
<?php include_once "shared/siteheader.php"; ?>
<?php include_once 'shared/header-top.php' ?>
<?php include_once 'shared/EnquiryFormSticky_view.php' ?>	

<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAhTk4FV3CNIPhLrbpj3f8vBJZn1PRAvXk'></script>
<script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=518e583194c347ef93cfecda3b76617f11b9fb97'></script>

<div class="container-fluid">
    <div class="row" style="padding:0px">
        <div class="col-sm-12 col-md-12">
	    <?php
	    if (isset($service_img->Upload_Image)) {
		?>
    	    <img src="<?php echo base_url() . $service_img->Upload_Image ?>" style="height: 200px; width:100%">
		<?php
	    }
	    ?>
        </div>
    </div>
</div>	

<div class="container-fluid">
    <div class="row">		

	<?php include_once 'shared/contact_label.php' ?>

        <div class="col-sm-12 col-md-12"> 
            <div class="col-sm-12 col-md-12 comp-aboutUs-page">

                <div class="col-sm-12 col-md-12" style="height:auto">

                    <div class="col-sm-2 col-md-2 title text-center"></div>
                    <div class="col-sm-2 col-md-2"><hr /></div>

                    <div class="col-sm-4 col-md-4">
                        <p class="title text-center">  Contact  <font color="#e49494">Us</font></p> 
                        <p class="text-center">"Offering complete pest control solution"</p>			
                    </div> 

                    <div class="col-sm-2 col-md-2"><hr /></div>
                    <div class="col-sm-2 col-md-2 title text-center"></div>	
                </div>

                <div class="col-sm-12 col-md-12 content "> 
		    <div class="col-lg-3 col-md-3 col-sm-3 col-lg-12">
			<?php include_once 'shared/left_nav_menu.php' ?>
		    </div>

                    <div class="col-lg-9 col-md-9 col-sm-9 col-lg-12">	
			<div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
			    <p style="font-size:18px; line-height:1.2; margin-top:0px;">
				<strong>vPestify</strong><br>Sector 39, Jharsa Village, <br />Bhardwaj Complex, <br>PIN CODE - 122022, Gurgaon<br>					
			    </p>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-lg-12">
			   <div style='overflow:hidden;height:500px;width:720px;'>
			   <div id='gmap_canvas' style='height:500px;width:720px;'></div>
			    <style>#gmap_canvas img{max-width:none!important;background:none!important ;} .linkmap { font-size:.5px;}</style></div>
				<a href='https://addmap.net/'><span class="linkmap">vpestify</span></a> 
			</div>
		    </div>
                </div>

                <div class="col-sm-12 col-md-12 text-center " style="height:5px;"></div>			
                <div class="col-sm-12 col-md-12 text-center ">			

                </div>			
            </div>
        </div>	

        <div class="col-sm-12 col-md-12" style="height:100px"> </div>

    </div>	<!-- row -->
</div><!-- container -->

<!-- FOOTER -->
<script type='text/javascript'>function init_map(){var myOptions = {zoom:12,center:new google.maps.LatLng(28.4416833,77.04997000000003),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(28.4416833,77.04997000000003)});infowindow = new google.maps.InfoWindow({content:'<strong>vPestify</strong><br>Bhardwaj Complex, Jharsa Village, Sector 39<br>122022 Gurgaon<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>		

<script>
    $(document).ready(function () {
	$('.nav .contact-us').addClass('active-tab');
   });
</script>
<?php include 'cstmCssJs/cstmJs.php' ?>	
<?php include_once "shared/SiteFooter.php"; ?>

