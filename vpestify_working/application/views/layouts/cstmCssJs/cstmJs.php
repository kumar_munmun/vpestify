<script type="text/javascript">
    $(document).ready(function () {
                $(window).scroll(function () {
                            $(".navbar-default").toggleClass(
                                    "navbar-shrink", $(
                                            this).scrollTop
                                    () > 50);
                            $(".navbar-default").toggleClass(
                                    "sizeOnScrollHeader", $(
                                            this).scrollTop() > 50);

                            if ($(this).scrollTop() > 100) {
                                $('#scrollToTop').fadeIn();
                                $('')
                            } else {
                                $('#scrollToTop').fadeOut();
                            }
                        });

                $('#scrollToTop').click(function () {
                            $("html, body").animate({scrollTop: 0}, 1000);
                            return false;
                });

                $(".scrolltodiv").click(function (e) {
                            e.preventDefault();
                            $('html, body').animate({
                                        scrollTop: $($.attr(this, 'href')).offset().top},
                            700);
                });

                /*------------------------Enquiry-Form-----------------------*/

                $("#enquiry_btn").click(function (e) {
                            e.preventDefault();
                            var user = $(
                                    "#user_name").val();
                            var email = $(
                                    "#email").val();
                            var contact = $(
                                    "#contact").val();
                            var aboutEnquiry = $(
                                    "#aboutEnquiry").val();

                            if (user == '' || user == 0)   {
                                $(".nameMsgshow").show();
                                $("#user_name").focus();
                                return false;
                            }

                            if (email == '' || email == 0){
                                $(".emailMsgshow").show();
                                $("#email").focus();
                                return false;
                            }

                            if (contact == '' || contact == 0) {
                                $(".contactMsgshow").show();
                                $("#contact").focus();
                                return false;
                            }

                            if (aboutEnquiry == '' || aboutEnquiry == 0) {
                                $(".aboutMsgshow").show();
                                $("#aboutEnquiry").focus();
                                return false;
                            }

                            $.ajax({
                                        url: "<?php echo base_url() ?>index.php/Admin/userSendEnquiry",
                                        type: "POST",
                                        data: {user: user, email: email, contact: contact, aboutEnquiry: aboutEnquiry},
                                        dataType: "text",
                                        success: function (response) {
                                            var msg = $.trim(
                                                    response);
                                            if (msg == 'Ok') {
                                                $('.textField').val('');
                                                $(".modal").modal('hide');
                                                alert("Your message has been sent.");
                                            }
                                        }
                                    });

                        });

                /*-----------------------Home-Enquiry-Form-----------------------*/

                $( ".enquiry_btnHome").click(function (e) {
                            e.preventDefault();
                            var user = $(
                                    ".user_name").val();
                            var email = $(
                                    ".email").val();
                            var contact = $(
                                    ".contact").val();
                            var aboutEnquiry = $(
                                    ".aboutEnquiry").val();

                            if (user == '' || user == 0) {
                                $(".nameMsgshow").show();
                                $("#user_name").focus();
                                return false;
                            }

                            if (email == '' || email == 0)  {
                                $(".emailMsgshow").show();
                                $("#email").focus();
                                return false;
                            }

                            if (contact == '' || contact == 0){
                                $(".contactMsgshow").show();
                                $("#contact").focus();
                                return false;
                            }

                            if (aboutEnquiry == '' || aboutEnquiry == 0) {
                                $(".aboutMsgshow").show();
                                $("#aboutEnquiry").focus();
                                return false;
                            }

                            $.ajax({
                                        url: "<?php echo base_url() ?>index.php/Admin/userSendEnquiry",
                                        type: "POST",
                                        data: {user: user, email: email, contact: contact, aboutEnquiry: aboutEnquiry},
                                        dataType: "text",
                                        success: function (response) {
                                            var msg = $.trim(
                                                    response);
                                            if (msg == 'Ok'){
                                                $('.textField').val(
                                                        '');
                                                $(".modal").modal('hide');
                                                alert("Your message has been sent.");
                                            }
                                        }
                                    });
                        });
                                                
                    // Home for testimonial
                     $(".testimonial-carousel").slick({
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true,
                        arrows: true,
                        prevArrow: $(".testimonial-carousel-controls .prev"),
                        nextArrow: $(".testimonial-carousel-controls .next")
                    });

            });

</script> 