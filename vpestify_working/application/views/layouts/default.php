<?php $loggedData = logged_user_data(); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title><?php echo isset($page_title) ? $page_title : ucfirst($this->uri->rsegments[1]); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <?php $this->load->view("elements/head"); ?>
    </head>

    <!-- <body class="page-header-fixed page-sidebar-fixed page-quick-sidebar-over-content page-style-square"> -->
    <body class="page-header-fixed page-quick-sidebar-over-content">
        <?php $this->load->view("elements/header", array('loggedData' => $loggedData)); ?>

        <div class="page-container">
            <?php $this->load->view("elements/leftbar", array('loggedData' => $loggedData)); ?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <?php $this->load->view($page_view, array('loggedData' => $loggedData)); ?>
                </div>
            </div>
        </div>
        <?php $this->load->view("elements/footer"); ?>
        <script>
            jQuery(
                    document).ready(
                    function () {
                        Metronic.init();
                        Layout.init();
                        QuickSidebar.init();
                        Demo.init();
                    });
        </script>
    </body>
</html>
