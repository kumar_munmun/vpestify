<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->helper('url');
?>
<?php include_once "shared/siteheader.php"; ?>

<?php include_once 'shared/header-top.php' ?>
<?php include_once 'shared/EnquiryFormSticky_view.php' ?>


<div class="container-fluid">
    <div class="row" style="padding:0px">
        <div class="col-sm-12 col-md-12">
	    <img src="<?php echo base_url() ?>assets/images/safety/bed1.png" style="height: 200px; width:100%">
        </div>
    </div>
</div>	

<div class="container-fluid">
    <div class="row">		
    <?php include_once 'shared/contact_label.php' ?>
        <div class="col-sm-12 col-md-12"> 
            <div class="col-sm-12 col-md-12 comp-aboutUs-page">

                <div class="col-sm-12 col-md-12" style="height:auto">

                    <div class="col-sm-2 col-md-2 title text-center"></div>
                    <div class="col-sm-2 col-md-2"><hr /></div>

                    <div class="col-sm-4 col-md-4">
                        <p class="title text-center">  vPestify  <font color="#e49494">Safety</font></p> 
                        <p class="text-center">"Offering complete pest control solution"</p>			
                    </div> 

                    <div class="col-sm-2 col-md-2"><hr /></div>
                    <div class="col-sm-2 col-md-2 title text-center"></div>	
                </div>

                <div class="col-sm-6 col-md-6 content text-justify"> 	
                   <p style="font-weight:bold; line-height:5px; line-height: 53px;">We use only Government Approved Insecticides</p>
				   <p style="font-size: 14px">  We use only Government Approved and environment friendly products 
				   		which are bio degradable/ less toxic and not a human hazard and others are as
 				   		stated and certified by the Govt. directives which are non smelly in nature. These medicine are in different
				   		forms such as powder, gel, paste, granules, cake, liquid, sticky pads etc.
				   </p>
				   <p>
				   How to prepare before Pest Control Treatment

 Open all curtains but tightly close the windows.
 All external doors should be closed but internal doors should be open.
 Keep pets outside and secure them.
 Cover fish tanks/bowls  with an impermeable sheet.
Secure children�s toys or remove them from areas to be treated..
Cover baby�s  bed. Safety for babies during Pest control is very important.
Cover or put all exposed food in refrigerator.
Kitchen counters where food is prepared should be thoroughly washed after the treatment.
Stay out of the house and keep it airtight for atleast 3 hours after the treatment. If someone in the family is sensitive to chemical odor, he/she must be away for at least 24 Hours.
Aerate the house thoroughly by opening the  windows/doors when you return after three or four hours.
When you return after the treatment, just vaccum the floor and avoid wet mopping for 24 hrs or it will remove the protection.
A thorough cleaning and washing of  kitchen utensils is must after Pest Control The above mentioned precautions or safety measures before Pest Control ensures efficacy of the treatment without compromising the health of your loved ones.
General Pest Control precautions

1). Do not eat or smoke while pest control is being carried out indoor.

2). Keep your pets away or relocate them while the pest control is being done. Most pesticides are harmful to birds and fish. Therefore bird cage or Aquarium must be relocated before the pest control be carried out

3). Cover food items or put them in refrigerator.  Keep utensils and other electrical gazetts away from the area to be treated.

4). Thoroughly clean kitchen counter and dining table before using them after the treatment is done.

5). The flow of the pesticide sprays should be directed towards wall floor junction along the skirting level. Avoid spraying all over and spilling pesticides unnecessarily.

Safety Precautions during outdoor Pest Control

The pesticides used outdoor are different from those used indoor. Normally the outdoor pest control applications are harder as they have to be resistant to rain and sunlight.

Here are some important precautions for outdoor pest control treatments.

 All doors and windows must be tightly closed before using outdoor pesticides so that the fumes don�t get drift inside.
Avoid getting outdoor pest control done on a rainy or the day when strong winds are blowing as weather has direct impact of efficacy of the treatment. Moreover outside  pest control on a rainy or windy day can prove to be hazardeous to pests , animals  people and the living environment.
 fish ponds, barbeques and vegetable gardens must be protected from any direct spraying and if possible be covered.
Outside pet beddings, dog-house or their water and food bowls must be removed or covered.
Avoid watering of your garden immediately after Outdoor pest control has been done. At least 6-8 hrs. of exposure period is essential after any kind of pest control treatment is carried done outside.
Keep plants and birds under observation  for a few days after pest control to make sure that the instecticide used are not causing harm to them . Some pest control products have a delayed side effect.
Inform your neighbours about pest control activity done to the external  areas.
If you have any  rainwater or water tank, make sure that the insecticide residue does not get washed into them as it may lead to severe contamination .
Who are more prone to Pesticides poisoning

If you are planning to get your premise Pesticide treatment done, following groups of people are more prone and sensitive to the treatment.But this list should not be considered as a rule of thumb, as anyone can be affected by pesticides.

unborn babies and young children.
pregnant and nursing women
Aged people.
Asthmatic and allergic patients.
People recently undergone surgical treatment.
Pests like fish and birds.
What to do after Pest control Treatment

It�s important to give it time for pest control to take effect. Most products need time to dry . Most products have longer residual affect, lasting up to 2-3 months if undisturbed. Therefore avoid mopping along the (edges)wall floor junction of the room where pesticides have been used. It will leave the treatment in place and yield better results.
				   </p>
                </div>

                <div class="col-sm-6 col-md-6 text-center ">
                   asfdsf

                </div>
				
                <div class="col-sm-12 col-md-12 text-center " style="height:50px;"></div>


            </div>
        </div>	



        <div class="col-sm-12 col-md-12" style="height:100px"> </div>

    </div>	<!-- row -->
</div><!-- container -->

<!-- FOOTER -->
<script>
   $(document).ready(function(){
   		$('.nav .safety').addClass('active-tab');
   });
</script>
<?php include 'cstmCssJs/cstmJs.php' ?>

<?php include_once "shared/SiteFooter.php"; ?>

