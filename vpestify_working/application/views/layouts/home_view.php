<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?>
<?php include_once "shared/siteheader.php"; ?>
<?php include_once 'shared/header-top.php' ?>
<?php include_once 'shared/EnquiryFormSticky_view.php' ?>

<div class="container-fluid">

    <?php /* ?><?php include 'subheadermenubar/SubHeaderMenu.php'?>	<?php */ ?>			

    <div class="row" style="padding:0px">
        <div class="col-sm-12 col-md-12 col-xs-12" > 
            <?php include "shared/bootstrap-slider.php" ?>
        </div>	
        <?php include 'shared/button_menu.php' ?>
    </div>
</div>		

<div class="container">
    <div class="row">				

        <div class="col-sm-12 col-md-12 col-xs-12"> 
            <div class="txt-title font-size comp-titl-content">

                <div class="col-sm-3 col-md-3 col-xs-12"></div>
                <div class="col-sm-2 col-md-2 col-xs-12"><hr /></div>

                <div class="col-sm-2 col-md-2"><p class="title text-center"> About <font color="#e49494">Us</font> </p></div> 

                <div class="col-sm-2 col-md-2 col-xs-12"><hr /></div>
                <div class="col-sm-3 col-md-3 col-xs-12"></div>	

                <div class="col-sm-12 col-md-12 col-xs-12 content text-justify"> 	
                    <p class="font-size15">
                        <?php
                        if (isset($rsType)) {
                            foreach ($rsType as $rws) {
                                if ($rws->Image_Type_Name == 'About') {
                                    echo $rws->ImageTypeContent;
                                }
                            }
                        }
                        ?>
                     </p>
                </div>
            </div>
        </div>	


    </div>	<!-- row -->
</div><!-- container -->

<div class="container-fluid">
    <div class="row">

        <div class="col-sm-12 col-md-12 col-xs-12" style="color: white"> 
            <section class="testimonials py-5 text-white px-1 px-md-5 margin-top-xl">
                
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="pt-2 text-center font-weight-bold">Our Customers Are Seeing Big Results</h2>

                            <div class="carousel-controls testimonial-carousel-controls">
                                <div class="control d-flex align-items-center justify-content-center prev mt-3" style="margin-top: 20px">
									<i class="fa fa-chevron-left" style="padding: 14px;"></i></div>
                                <div class="control d-flex align-items-center justify-content-center next mt-3" style="margin-top: 20px">
									<i class="fa fa-chevron-right" style="padding: 14px 14px 14px 17px;"></i></div>

                                <div class="testimonial-carousel">
                                    <div class="h5 font-weight-normal one-slide mx-auto">
                                        <div class="testimonial w-100 px-3 text-center d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                            <div class="message text-center blockquote w-100">"They’ve been consistent throughout the years and grown together with us. 
											Even as they’ve grown, they haven’t lost sight of what they do. Most of their key resources are still with them, which is also 
											a testament to their organization."</div>
                                            <div class="blockquote-footer w-100 text-white">Ted, WebCorpCo</div>
                                        </div>
                                    </div>
                                    <div class="h5 font-weight-normal one-slide mx-auto">
                                        <div class="testimonial w-100 px-3 text-center  d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                            <div class="message text-center blockquote w-100">"Our website uses Solodev to craft a website capable of representing its 
											diverse residents. The website features a newsroom with the latest events, an interactive calendar, and a mobile app that puts 
											the resources at a user’s fingertips."</div>
                                            <div class="blockquote-footer w-100 text-white">Jim Joe, WebCorpCo</div>
                                        </div>
                                    </div>
                                    <div class="h5 font-weight-normal one-slide mx-auto">
                                        <div class="testimonial w-100 px-3 text-center  d-flex flex-direction-column justify-content-center flex-wrap align-items-center">
                                            <div class="message text-center blockquote w-100">
											Solodev is a great company to partner with! We are extremely happy with the software, service, and support.</div>
                                            <div class="blockquote-footer w-100 text-white">Jim Joe, WebCorpCo</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-8 col-md-8 col-xs-12"> 
            <div class="imageDiscount col-sm-12 col-md-12 col-xs-12" style="width:100%; height:300px; margin-top:5px">
                <img class="" src="<?php echo base_url() ?>/assets/images/discount-price-tags.png" style="height:290px; width:90%; " />
            </div>

            <div class="col-sm-12 col-md-12 col-xs-12" style="width:100%; padding:0; height:auto; margin-top:5px">
                <div class="col-sm-12 col-md-6 col-xs-12">
                    <img class="" src="<?php echo base_url() ?>/assets/images/WOOD3.jpg" style="height:190px; width:100%; " />
                </div>
                <div class="col-sm-12 col-md-6 col-xs-12">
                    <img class="" src="<?php echo base_url() ?>/assets/images/bed3.jpg" style="height:190px; width:100%; " />
                </div>			
            </div>

        </div>	

        <div class="col-sm-4 col-md-4 col-xs-12"> 
            <div class="contact-form">

                <p class="text-center enquiry-title">Enquiry Form</p>
                <div class="col-md-12 col-sm-12 col-xs-12 cstmInputs">
                    <input name="user_name" placeholder="Your Name" class="form-control textField user_name" type="text">
                    <span class="nameMsgshow requiredMsg">! Enter your name</span>
                </div>
                <div class="col-md-12 col-sm-12 cstmInputs">
                    <input name="user_email" placeholder="Your Email" class="form-control textField email" type="text">
                    <span class="emailMsgshow requiredMsg">! Enter your email</span>
                </div>
                <div class="col-md-12 col-sm-12 cstmInputs">
                    <input name="contact" placeholder="Your Contact" class="form-control textField contact" maxlength="10" type="text">
                    <span class="contactMsgshow requiredMsg">! Enter your contact</span>
                </div>
                <div class="col-md-12 col-sm-12 cstmInputs">
                    <textarea name="aboutEnquiry" placeholder="About Enquiry Details" class="form-control textField aboutEnquiry" type="text" style="height:148px !important">
					</textarea>	
                    <span class="aboutMsgshow requiredMsg">! Enter your details</span>			
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <span id="enquiry_btn" class="btn btn-info form-control enquiry_btnHome">Send Enquiry</span>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
   $(document).ready(function(){
   		$('.nav .home').addClass('active-tab');
   });
</script>
<!-- FOOTER -->
<?php include 'cstmCssJs/cstmJs.php' ?>	
<?php include_once "shared/SiteFooter.php"; ?>

