<div class="cutting-edge" style="height:5px; background:green; margin-top:10px;"></div>

<div id="custmContainerFluid" class="container-fluid" style="background:#333333; width:100%">
    <div class="row" style="padding-top:18px; width:100%">

        <div class="col-md-3 cstmFooterContent"> 
            <div  class="cstmFooterHeader"><a class="lnkFooterHome" href="<?php echo site_url('Home/index') ?>">Home</a></div>
            <ul class="ulList">
                <li><a class="Link-text-centered Link-fontcolor Link-animated Link-fadeInRight" href="" ></a> </li>
            </ul>
        </div>

        <div class="col-md-3 cstmFooterContent" > 
            <div  class="cstmFooterHeader">Contact Us</div>
            <ul class="ulList">
                <li><a href="<?php echo site_url("contactus") ?>" class="Link-text-centered Link-fontcolor Link-animated Link-fadeInRight" href="#">Contact</a>  </li>
            </ul>
        </div>

        <div class="col-md-3 cstmFooterContent">
            <div  class="cstmFooterHeader">Services</div>
            <ul class="ulList">	
                <?php
                if (isset($rsServices)) {
                    foreach ($rsServices as $rws) {
                        $serviceContent = str_word_count($rws->Service_Content);
                        $service_name = preg_replace("/\s+/", "-", $rws->Service_Name);
                        ?>
                        <li><a href="<?php echo site_url('vpestifyservices/' . $service_name . '') ?>"
                               class="Link-text-centered Link-fontcolor Link-animated Link-fadeInRight"><?php echo ucwords($rws->Service_Name); ?></a></li>	
        <?php
    }
}
?>							
            </ul>
        </div>

        <div class="col-md-3 cstmFooterContent"> 
            <div  class="cstmFooterHeader">Follow Us</div>
            <ul class="ulList followusFooter" style="margin-top:37px; padding-left:60px; font-size:0px !important">
                <li>			  
                    <a data-original-title="Facebook" href="https://m.www.facebook.com/" target="_blank" class="ic1" data-toggle="tooltip"
                       data-placement="bottom" title=""><i class="fa fa-facebook "></i></a>

                    <a data-original-title="Twitter" href="https://twitter.com/" target="_blank" class="ic1" data-toggle="tooltip" data-placement="bottom" title="">
                        <i class="fa fa-twitter "></i></a>

                    <a data-original-title="Google-plus" href="https://plus.google.com/" target="_blank" class="ic1" 
                       data-toggle="tooltip" data-placement="bottom" title=""><i class="fa fa-google-plus "></i></a>

                    <a data-original-title="LinkedIn" href="http://in.linkedin.com/" target="_blank" class="ic1" data-toggle="tooltip" data-placement="bottom" title="">
                        <i class="fa fa-linkedin "></i></a>
                </li>	

            </ul> 
        </div> 	

        <div class="col-md-12 col-sm-12 col-xs-12" class="FooterContactSection">
            <div class="col-md-12 col-sm-12 col-xs-12" style="border-top:1px solid #514747; padding:7px;"></div>
            <div class="col-md-8 col-sm-12 col-xs-12 footerLinkContact"> Mobile Number : 9806161974 , &nbsp;&nbsp; Email Address : vpestify@gmail.com </div>
            <div class="col-md-4 col-sm-12 col-xs-12 footerLinkContact"> &copy; 2017 <a href="#" style="color: #1b98a4">vPestify</a>. All rights reserved. </div>
                    <!--<p class="copyright">&copy; 2017 <a href="#">Advenjour</a>. All rights reserved.</p> -->
        </div>				

    </div>
</div> 	
