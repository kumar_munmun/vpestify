﻿<div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto; width: 1140px; height: 250px; overflow: hidden;">           
    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
        <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;  background-color: #000; top: 0px; 
             left: 0px;width: 100%; height:100%;">    </div>
        <div style="position: absolute; display: block; background: url(assets/icons/loading.gif) no-repeat center center; top: 0px; left: 0px;
             width: 100%;height:100%;"></div>
    </div>
    <!-- Slides Container -->
    <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1140px; height: 250px; overflow: hidden;">

        <?php
        if (isset($res_set)) {
            $Upload_Image[] = '';
            foreach ($res_set as $rws) {
                if ($rws->Image_Place == 'slider') {
                    $Upload_Image = explode(',', $rws->Upload_Image);
                }
            }
        }

        for ($i = 0; $i < count($Upload_Image); $i++) {
            ?>
            <div><img u="image" src2="<?php echo base_url() . $Upload_Image[$i] ?>" />ss2</div>

            <?php
        }
        ?>

    </div>

    <style>               
        .jssorb05 {  position: absolute; }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av { position: absolute;   /* size of bullet elment */
                                                            width: 16px;   height: 16px;  background: url(assets/icons/b05.png) no-repeat; overflow: hidden; cursor: pointer;}
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
    </style>
    <!-- bullet navigator container -->
    <div u="navigator" class="jssorb05" style="bottom: 16px; right: 6px;">
        <!-- bullet navigator item prototype -->
        <div u="prototype"></div>
    </div>

    <style>               
        .jssora11l, .jssora11r { display: block; position: absolute; /* size of arrow element */
                                 width: 37px;  height: 37px; cursor: pointer; background: url(assets/icons/a11.png) no-repeat; overflow: hidden; }
        .jssora11l { background-position: -11px -41px; }
        .jssora11r { background-position: -71px -41px; }
        .jssora11l:hover { background-position: -131px -41px; }
        .jssora11r:hover { background-position: -191px -41px; }
        .jssora11l.jssora11ldn { background-position: -251px -41px; }
        .jssora11r.jssora11rdn { background-position: -311px -41px; }
    </style>
    <!-- Arrow Left -->
    <span u="arrowleft" class="jssora11l" style="top: 123px; left: 8px;">  </span>           
    <span u="arrowright" class="jssora11r" style="top: 123px; right: 8px;">  </span>

</div>

<!-- Jssor Slider End -->
<!-- </div>-->
<!--</div>
</div>
</div>-->

<!-- Bootstrap core JavaScript
================================================== -->  
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jssor.slider.mini.js"></script>

<script>

    jQuery(
            document).ready(
            function ($) {
                var options = {
                    $AutoPlay: true,
                    $AutoPlaySteps: 1,
                    $AutoPlayInterval: 2000,
                    $PauseOnHover: 1,
                    $ArrowKeyNavigation: true,
                    $SlideEasing: $JssorEasing$.$EaseOutQuint,
                    $SlideDuration: 800,
                    $MinDragOffsetToSlide: 20,
                    //$SlideWidth: 600,                                 
                    $SlideHeight: 250,                                
                    $SlideSpacing: 0,
                    $DisplayPieces: 1,
                    $ParkingPosition: 0,
                    $UISearchMode: 1,
                    $PlayOrientation: 1,
                    $DragOrientation: 1,

                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$,
                        $ChanceToShow: 2,
                        $AutoCenter: 2,
                        $Steps: 1,
                        $Scale: false
                    },

                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$,
                        $ChanceToShow: 2,
                        $AutoCenter: 1,
                        $Steps: 1,
                        $Lanes: 1,
                        $SpacingX: 12,
                        $SpacingY: 4,
                        $Orientation: 1,
                        $Scale: false
                    }
                };

                var jssor_slider1 = new $JssorSlider$("slider1_container", options);

                //responsive code begin
                //you can remove responsive code if you don't want the slider scales while window resizes
                function ScaleSlider() {
                    var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                    if (parentWidth) {
                        jssor_slider1.$ScaleWidth(parentWidth - 30);
                    } else
                        window.setTimeout(ScaleSlider, 30);
                }
                ScaleSlider();

                $(window).bind( "load", ScaleSlider);
                $(window).bind("resize", ScaleSlider);
                $(window).bind("orientationchange", ScaleSlider);
                //responsive code end
            });
</script>