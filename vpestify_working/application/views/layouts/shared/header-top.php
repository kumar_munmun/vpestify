</head>

<body>
    <div class="container-fluid">
        <div class="row top-header">

            <div class="col-md-9 col col-sm-12 col-xs-12 text-center">						 
                <div class="contact-class"> <i class="fa fa-phone" aria-hidden="true"></i> +91 9806161974 &nbsp;&nbsp;<span class="fa fa-envelope" aria-hidden="true"></span>
                    vpestify@gmail.com</div>				
            </div>

            <div class="col-md-3 col col-sm-12 col-xs-12 text-right">

                <div style="width:100%; height:auto">
                    <div style="width:100%;  height:auto; padding-top:5px; margin-bottom:5px;">
                        <div class="icons tooltip-examples">                
                            <a data-original-title="Facebook" href="https://m.www.facebook.com/" target="_blank" class="ic1" data-toggle="tooltip"
                               data-placement="bottom" title=""><i class="fa fa-facebook "></i></a>

                            <a data-original-title="Twitter" href="https://twitter.com/" target="_blank" class="ic1" data-toggle="tooltip" data-placement="bottom" title="">
                                <i class="fa fa-twitter "></i></a>

                            <a data-original-title="Google-plus" href="https://plus.google.com/" target="_blank" class="ic1" 
                               data-toggle="tooltip" data-placement="bottom" title=""><i class="fa fa-google-plus "></i></a>

                            <a data-original-title="LinkedIn" href="http://in.linkedin.com/" target="_blank" class="ic1" data-toggle="tooltip" data-placement="bottom" title="">
                                <i class="fa fa-linkedin "></i></a>
                        </div>				
                    </div>
                </div>					
            </div>

            <!--<div class="col-md-1 col col-sm-12 col-xs-12"></div>-->
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-3 col-md-3 col-xs-12">
                <div style="text-align:center; width:100%; padding:8px;">			
                    <a href="<?php echo site_url('pestcontrol/') ?>" style="text-decoration: none;">
                        <img src="<?php echo base_url() ?>assets/icons/logo.jpg" style="height: 52px; width: 200px">
		            </a>
                    <div style="font-size: 13px;">Here Easy To Being Hygiene</div>
                </div>			
            </div>

            <div class="col-sm-9 col-md-9 col-xs-12 text-center padding-top" >			 
                   
                <a class="toggleMenu" href="#">Menu</a>
                <ul class="nav">
                    <li class="home"><a href="<?php echo site_url('pestcontrol') ?>">Home  </a></li>	
                    <li class="contact-us"><a href="<?php echo site_url('contactus') ?>">Contact Us  </a></li>
                    <li class="about-us"><a href="<?php echo base_url('aboutus') ?>">About Us   </a></li>
                    <li class="safety"><a href="<?php echo base_url('safety') ?>">Safety  </a></li>
                    <li class="dropdown">
            			<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <b class="caret"></b></a>
            			<ul class="dropdown-menu">
            
            			    <?php
            			    if (isset($rsServices)) {
            				foreach ($rsServices as $rws) {
            				    $serviceContent = str_word_count($rws->Service_Content);
            				    $service_name = preg_replace("/\s+/", "-", $rws->Service_Name);
            				    ?>
            				    <li><a href="<?php echo site_url('vpestifyservices/' . $service_name . '') ?>">
            					    <?php echo ucwords($rws->Service_Name); ?></a>
            				    </li>
            				    <li class="divider"></li>
            				    <?php
            				}
            			    }
            			    ?>				    
            			</ul>
            		</li>	
                </ul>
            </div>
           
            <a href="#" id="scrollToTop" title="Scroll to Top" style="display: none; z-index:9999"><span>Top</span></a>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/subheadermenubar/script.js"></script>