<div class="col-sm-12 col-md-12 col-xs-12">

    <div class="col-sm-12 col-md-12 col-xs-12 button_menu">

        <div class="col-sm-12 col-md-12 col-xs-12" style="height:auto">
            <div class="col-sm-2 col-md-2 col-xs-12 title text-center"></div>
            <div class="col-sm-2 col-md-2 col-xs-12"><hr /></div>

            <div class="col-sm-4 col-md-4 col-xs-12">
                <p class="title text-center">Services <font color="#e49494">Offered</font></p> 
                <p class="text-center">"Offering complete pest control solution"</p>			
            </div> 

            <div class="col-sm-2 col-md-2 col-xs-12"><hr /></div>
            <div class="col-sm-2 col-md-2 col-xs-12 title text-center"></div>	
        </div>

	<?php
	if (isset($rsServices)) {
	    foreach ($rsServices as $rws) {
		$serviceContent = str_word_count($rws->Service_Content);
		$Service_Name = preg_replace("/\s+/", "-", $rws->Service_Name);
		?>
		<div class="col-sm-6 col-md-3 col-xs-12 bdr_right">
		    <div class="text-centered animated bounceIn">
			<a href="<?php echo site_url('vpestifyservices/' . $Service_Name . '') ?>" class="white_icon">
			    <img src="<?php echo base_url() . $rws->Service_Image ?>" height="70px" width="70px" /></a>
		    </div>
		    <h2 class="text-centered fontcolor animated fadeInRight p-title">&nbsp;<?php echo ucwords($rws->Service_Name) ?></h2>
		    <div class="animated service-content"> <?php echo $rws->Service_Content ?>  </div>
		    <div class="readMore"><a href="<?php echo site_url('vpestifyservices/' . $Service_Name . '') ?>">Read More</a></div>
		</div>
		<?php
	    }
	}
	?>		  
    </div>   
</div>  
