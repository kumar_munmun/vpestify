<div class="left_nav">
    <ul>
	<li class="left_menu_title text-center" style="padding:3% 2%;"> vPestify Services </li>
	<?php
	if (isset($rsServices)) {
	    foreach ($rsServices as $rws) {
		$service_name = preg_replace("/\s+/", "-", $rws->Service_Name);
		?>
		<li><a href="<?php echo site_url('vpestifyservices/' . $service_name . '') ?>">
			<?php echo ucwords($rws->Service_Name); ?></a>
		</li>

		<?php
	    }
	}
	?>
	<li class="safety"><a href="<?php echo base_url('safety') ?>">Safety  </a></li> 	
	<li class="divider"></li>
    </ul>
</div>
