<style>	
    #feedback {
        position: fixed;
        right: 0;
        top: 36%;
        height: auto;
        /*margin-left: -3px;*/
        margin-bottom: -3px;
        z-index: 999999999999999;

    }
    .SideBtn {
        display: inline-block;
        padding: 5px 9px;
        /*padding:6px 15px 9px 6px;*/
        margin-bottom: 0;
        font-size: 19px;
        font-weight: 700;
        line-height: 18px;
        text-align: center;
        white-space: nowrap;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 0;

        border-radius: 4px;
        background:blue;

    }
    /*.modal-footer {  padding: 1 !important; }*/
    .popupImageLeft {text-align:center; height:400px; padding-left: 0px; background-image:url(<?php echo base_url() ?>assets/images/WOOD3.jpg) ; 
                     -webkit-background-size: cover;
                     -moz-background-size: cover;
                     -o-background-size: cover;
                     background-size: cover;

    }
    .innerEnquiryTitles {	text-align:center; font-size:18px; color: #f98012; padding:10px; border-bottom:1px solid #CCC}
    .innerEnquiryForm { height:auto; padding:10px   }
    .btn_enquiry { padding: 10px 17px 10px 23px;	}
</style>

<div id="feedback">
    <div id="feedback-tab"> 
        <button type="button" class="SideBtn" data-toggle="modal" data-target="#enquriFormPopup" style="color:white;">
            E<br />n<br />q<br />u<br />i<br />r<br />y</button>
    </div>
</div>

<div class="modal fade" id="enquriFormPopup" tabindex="-1">		  
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style=" margin-top:100px !important; ">

            <!--<button class="close" data-dismiss="modal" type="button" style="padding: 6px;">x</button>-->
            <div class="col-sm-5 col-md-5 popupImageLeft" style=""> </div>

            <div class="col-sm-7 col-md-7" style="height:400px;"> 
                <div class="innerEnquiryTitles"> <i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp; &nbsp;&nbsp; Enquiry Form </div>
                <div class="innerEnquiryForm">
                    <?php //echo form_open();?>
                    <input class="form-control textField" id="user_name" type="text" onKeyPress="return lettersOnly(
                                                                  event)" name="name" placeholder="Your Name" autofocus  >
                    <span class="nameMsgshow requiredMsg">! Enter your name</span>

                    <input class="form-control textField" id="email" type="email" name="email" placeholder="Your Email"  >
                    <span class="emailMsgshow requiredMsg">! Enter your email</span> 

                    <input class="form-control textField" id="contact" type="text" name="contact" maxlength="10" placeholder="Your Contact"  >
                    <span class="contactMsgshow requiredMsg">! Enter your contact</span>		

                    <textarea class="enquiryMessage textField" id="aboutEnquiry" style="height:100px; width:100%; padding: 8px;" placeholder=" Your Message" ></textarea> 
                    <span class="aboutMsgshow requiredMsg">! Enter your details</span>

                    <div style="padding:20px; float:right;">							  	
                        <input class="btn btn-primary btn-close" id="enquiry_btn" type="submit" name="enquiry_btn" value="Enquiry" 
                               style="padding: 5px; height: 35px; font-size: 17px;">

                        <input class="btn btn-danger btn-close" data-dismiss="modal" type="submit" name="btn_register"  value="Close">							  
                    </div>
                    <?php //echo form_close();?>
                </div>
            </div>										

            <div class="modal-footer" style="padding:0px">	</div>
        </div>
    </div>
</div>