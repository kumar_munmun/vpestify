<html>
    <head>
        <title>Pest Control</title>
        <meta content="Pest control" name="description"/>
        <meta content="cockrach, woodborn" name="keywords"/>
        <meta content="vcare" property="og:title"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="icon" href="<?php echo base_url(); ?>assets/icons/favicon.png">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom_css.css" type="text/css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.css" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/subheadermenubar/style.css">
        <link href="<?php echo base_url(); ?>assets/css/yamm.css" rel="stylesheet">
        
        <link href="<?php echo base_url(); ?>assets/css/slick-min-testimonial.css" rel="stylesheet">
        
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/moment.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
        
        <script src="<?php echo base_url(); ?>assets/js/slick-min-testimonial.js" type="text/javascript"></script>

