<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->helper('url');
?>
<?php include_once "shared/siteheader.php"; ?>

<?php include_once 'shared/header-top.php' ?>
<?php include_once 'shared/EnquiryFormSticky_view.php' ?>


<div class="container-fluid">
    <div class="row" style="padding:0px">
        <div class="col-sm-12 col-md-12">
	    <?php
	    if (isset($service_img->Upload_Image)) {
		?>
    	    <img src="<?php echo base_url() . $service_img->Upload_Image ?>" style="height: 200px; width:100%">
		<?php
	    }
	    ?>
        </div>
    </div>
</div>	

<div class="container-fluid">
    <div class="row">	       
	<?php include_once 'shared/contact_label.php' ?>	

        <div class="col-sm-12 col-md-12"> 
            <div class="col-sm-12 col-md-12 comp-aboutUs-page">

                <div class="col-sm-12 col-md-12" style="height:auto">

                    <div class="col-sm-2 col-md-2 title text-center"></div>
                    <div class="col-sm-2 col-md-2"><hr /></div>

                    <div class="col-sm-4 col-md-4">
                        <p class="title text-center">  About  <font color="#e49494">Our Company</font></p> 
                        <p class="text-center">"Offering complete pest control solution"</p>			
                    </div> 

                    <div class="col-sm-2 col-md-2"><hr /></div>
                    <div class="col-sm-2 col-md-2 title text-center"></div>	
                </div>

		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
		    <?php include_once 'shared/left_nav_menu.php' ?>
		</div>
		
                <div class="col-sm-9 col-md-9 ol-sm-9 col-xs-12 content text-justify"> 	
                    <p class="font-size15">
			<?php
			if (isset($result_set)) {

			    $Upload_Image = explode(",", @$result_set->Upload_Image);
			    echo $result_set->ImageTypeContent;
			}
			?>
		    </p>	
                </div>

                <div class="col-sm-12 col-md-12 text-center " style="height:50px;"></div>

                <div class="col-sm-12 col-md-12 text-center ">
		    <?php
		    if (count($Upload_Image) > 0) {
			foreach ($Upload_Image as $imageval) {
			    ?>
			    <div class="col-sm-6 col-md-4 col-xs-12 text-center ">
				<div class="image-aboutus"><img src="<?php echo base_url() . @$imageval ?>" /></div>	
			    </div>
			    <?php
			}
		    }
		    ?>
                </div>

            </div>
        </div>	

        <div class="col-sm-12 col-md-12" style="height:100px"> </div>

    </div>	<!-- row -->
</div><!-- container -->

<!-- FOOTER -->
<script>
    $(
	    document).ready(
	    function () {
		$(
			'.nav .about-us').addClass(
			'active-tab');
	    });
</script>
<?php include 'cstmCssJs/cstmJs.php' ?>
<?php include_once "shared/SiteFooter.php"; ?>

