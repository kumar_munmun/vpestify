<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->helper('url');
?>
<?php include_once "shared/siteheader.php"; ?>

<?php include_once 'shared/header-top.php'; ?>
<?php include_once 'shared/EnquiryFormSticky_view.php'; ?>

<div class="container-fluid">
    <div class="row" style="padding:0px">
        <div class="col-sm-12 col-md-12">
	    <?php
	    if (isset($service_img->Upload_Image)) {
		?>
    	    <img src="<?php echo base_url() . $service_img->Upload_Image ?>" style="height: 200px; width:100%">
		<?php
	    }
	    ?>
        </div>
    </div>
</div>		

<div class="container-fluid">
    <div class="row">		
        <?php include_once 'shared/contact_label.php' ?>

        <div class="col-sm-12 col-md-12"> 
            <div class="col-sm-12 col-md-12 comp-service-page">

                <div class="col-sm-12 col-md-12" style="height:auto">

                    <div class="col-sm-1 col-md-1 title text-center"></div>
                    <div class="col-sm-2 col-md-2"><hr /></div>

                    <div class="col-sm-6 col-md-6">
                        <p class="title text-center">Service <font color="#e49494"><?php echo ucwords(@$service_set->Service_Name); ?></font></p> 
                        <p class="text-center">"Offering complete pest control solution"</p>			
                    </div> 

                    <div class="col-sm-2 col-md-2"><hr /></div>
                    <div class="col-sm-1 col-md-1 title text-center"></div>	
                </div>

                <div class="col-sm-8 col-md-8 content text-justify" style="font-size: 14px"> 
                <img src="<?php echo base_url() . @$service_set->Service_Image ?>" style="height:20%; float:left; width:20%" />
                    <p class="">
            			<?php
            			if (isset($service_set)) {
            			    echo $service_set->Service_Content;
            			}
            			?>
            	   </p>
                </div>
                <div class="col-sm-4 col-md-4 text-center ">
                    
                </div>

            </div>
        </div>	

        <div class="col-sm-12 col-md-12" style="height:100px"> </div>

    </div>	<!-- row -->
</div><!-- container -->

<!-- FOOTER -->
<?php include 'cstmCssJs/cstmJs.php' ?>

<?php include_once "shared/SiteFooter.php"; ?>

