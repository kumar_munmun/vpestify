/*------------------------------------------------------------------------------------
 	Program Name            : Common JavaScript Functions
 	Created On            	: 22 Oct 2012
 	Created By            	: http://cqs.in, satyendra@ccwm.co
 	Note 		            : Some rights reserved. Please consult us
 							  before applying the code to your pages.
 /*------------------------------------------------------------------------------------*/

function ob(id)
{
	var obj
	if(obj = document.getElementById(id))	
		return obj;
	else
		return false;
}

function isEmail(email)
{
	if(/^[a-zA-Z]([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email) == false)	
		return false;
	else 
		return true
}

function setBlank(obj,txt)
{
	obj.value=obj.value!=txt?obj.value:"";
}

function setText(obj,txt)
{
	obj.value=obj.value==""?txt:obj.value;
}

function setSelect(id, Value) 
{
	  SelectObject=document.getElementById(id);
	  for(index = 0; index < SelectObject.length; index++) 
	  {
		   if(SelectObject[index].value == Value)
		   {
			 SelectObject.selectedIndex = index;
		   }
	   }
}

function getExt(filename)
{
	return /[^.]+$/.exec(filename);
}

function googleMap(Lat,Long,MapBoxId,Zoom,Icon,Title,MinZoom,MaxZoom)
{
	/* key=AIzaSyD96BPnbuxiIt1SwcfX_ZP3I9C5vn5TBRI */
	/* include this javascript in your page */
	/* <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script> */
	
	Lat=Lat*1;
	Long=Long*1;
	Zoom=Zoom?Zoom:8;
	Icon=Icon?Icon:'';
	Title=Title?Title:'';
	MinZoom=MinZoom?MinZoom:'';
	MaxZoom=MaxZoom?MaxZoom:'';
	
	var mapOptions = {
	  center: new google.maps.LatLng(Lat,Long),
	  zoom: Zoom,
	  minZoom: MinZoom,
	  maxZoom: MaxZoom,
	  
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var mymap = new google.maps.Map(document.getElementById(MapBoxId),mapOptions);
	
	var marker = new google.maps.Marker({
		position:new google.maps.LatLng(Lat,Long),
		title:Title,
		icon:Icon,
		map:mymap
	});
}


function getDistance(lat1,lon1,lat2,lon2)
{
	var R = 6371; // km
	var dLat = (lat2-lat1)*Math.PI / 180;
	var dLon = (lon2-lon1)*Math.PI / 180;
	var lt1 = lat1*Math.PI / 180;
	var lt2 = lat2*Math.PI / 180;
	
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lt1) * Math.cos(lt2);
			 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c;
	return d;
}