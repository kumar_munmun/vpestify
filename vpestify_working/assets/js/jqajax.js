function ajax(file_url,target,form,callback,method,async,refres)
{
	var asyn=true,fd="";
	var methode_type="POST";
	
	if(method)
		methode_type=method;
	
	if(async===false)
		asyn=false;
	
	if(form)
	{
		fd=jQuery('#'+form).serialize();
	}
	
	jQuery.ajax({
	  type: methode_type,
	  url: file_url,
	  data: fd,
	  async: asyn,
	  cache: false
	}).done(function( msg ) {
		if(form)
		{
			if(refres)
			{
				jQuery('#'+form).each(function(){this.reset()});
			}
		}
		if(target)
		{
			jQuery('#'+target).html(msg);
		}
		if(callback)
		{
		  eval(callback);
		}
	});
}

function fileAjax(file_url,target,form,callback)
{
	fd="";
	if(form)
	{
		var myForm = document.forms[form];
		fd = new FormData(myForm);
	}
	
	jQuery.ajax({
	  type: "POST",
	  url: file_url,
	  processData: false,
	  contentType: false,
	  data: fd,
	  cache: false
	}).done(function( msg ) {
		if(target)
		{
			jQuery('#'+target).html(msg);
		}
		if(callback)
		{
		  eval(callback);
		}
	});
}


function fileAjax_old(file_url,target_id,frm,callback,progressFunction,fileIndex)
{
	var total;
	fd=false;
	if(frm)
	{
		var myForm = document.forms[frm];
		var fd = new FormData(myForm);
	}
	
	if(!fileIndex)
		fileIndex=0; /* if form has more than one file input then index of to be uploaded file input */
	
	var xhr = new XMLHttpRequest();
	
	if(progressFunction)
	{
		xhr.file = jQuery('#'+frm+' input[type="file"]')[fileIndex].files[0]; // not necessary if you create scopes like this
		if(xhr.file)
		{
			if(xhr.upload) {
				xhr.upload.onprogress = function(e) {
					if (e.lengthComputable)
					{
						var done = e.loaded, total = e.total;
						eval(progressFunction+'('+Math.floor((done/total)*100)+')');
					}
				};
			}
		}
	}
	
	xhr.onreadystatechange=function()
	{
		if (xhr.readyState==4 && xhr.status==200)
		{
			if(progressFunction)
			{
				if(xhr.file)
				{
					eval(progressFunction+'('+100+')');	
				}
			}
			
			if(target_id)
				document.getElementById(target_id).innerHTML=xhr.responseText;
			
			if(callback)
				eval(callback);
		}
	}
	
	xhr.open("POST",file_url,true);
	xhr.send(fd);
}

