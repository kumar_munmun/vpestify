function setTabBox() {
	$(".tabBox .tabBtns a").click(function(e) {
		e.preventDefault();
		tabObj=$(this).parent().parent();
		$(".tabBtns a", tabObj).removeClass('act');
		$(this).addClass('act');
		
		index=$(this).index();
		$(".tabContentBx .tabContent", tabObj).hide();
		$(".tabContentBx .tabContent", tabObj).eq(index).show();
	});
}
	
$(document).ready(function(){
	$("[page-link]").click(function(){
		if($.trim($(this).attr('page-link')))
			location.href=$(this).attr('page-link');
	});
	
	$('[hide]').each(function(){
		t=$(this).attr('hide');
		if(!isNaN(t)){
			$(this).delay(t).fadeOut();
		}
	});
	
	//$('[center="true"]').center(true);
	
	
	$(".delLink").click(function(e){
		e.preventDefault();
		delMsg=$(this).attr("delMsg");
		if(!delMsg)
			delMsg="Are you sure to delete?";
		if(confirm(delMsg))
		{
			url=$(this).attr("href");
			location.href=url;
		}
	});
	
	$(".oddEven tr:odd").not('.h').addClass("c1");
	$(".oddEven tr:even").not('.h').addClass("c2");
	
	$(".popupLink").click(function(e){
		e.preventDefault();
		url=$(this).attr("href");
		popupAjax(url, ''+$(this).attr('popupTitle')+'', ''+$(this).attr('popupWidth')+'');
	});
	
	/** Tab **/
	setTabBox();
});