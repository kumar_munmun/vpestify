$(function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      show: {
        duration: 1000
      },
      hide: {
        duration: 1000
      },
      buttons: {
        "yes": function() {
          $( this ).dialog( "close" );
        },
        No: function() {
          $( this ).dialog( "close" );
        }
      }
    });
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });
  });
  