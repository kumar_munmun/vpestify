jQuery.fn.center = function (notTopBtm) {
	if(this.css("position")!='relative' && this.css("position")!='fixed')
    	this.css("position","absolute");
	
	$top=Math.max(0, ((jQuery(window).height() - this.outerHeight()) / 2) + jQuery(window).scrollTop());
   	
	if(!notTopBtm)
		this.css("top", $top+"px");
		
	this.css("left", Math.max(0, ((jQuery(window).width() - this.outerWidth()) / 2) + jQuery(window).scrollLeft()) + "px");
                                                
    return this;
}

jQuery.fn.fromTop = function (arg,leftcentre) {
    this.css("position","absolute");
    this.css("top", Math.max(0, arg +  jQuery(window).scrollTop()) + "px");
     
	if(leftcentre)
	{                                         
    	this.css("left", Math.max(0, ((jQuery(window).width() - this.outerWidth()) / 2) + jQuery(window).scrollLeft()) + "px");
	}
                                                
    return this;
}

jQuery(document).ready(function(){
	jQuery("input,textarea").focus(function(){jQuery(this).attr('focused','true')});
	jQuery("input,textarea").click(function(){jQuery(this).attr('focused','true')});
	jQuery("input,textarea").blur(function(){jQuery(this).removeAttr('focused')});
	jQuery.fn.isFocused=function(){return jQuery(this).attr('focused')=='true'?true:false}
});

jQuery.fn.selectRange = function(start, end) {
  return this.each(function() {
	if (this.setSelectionRange) { this.setSelectionRange(start, end);
	} else if (this.createTextRange) {
		var range = this.createTextRange();
		range.collapse(true); 
		range.moveEnd('character', end); 
		range.moveStart('character', start); 
		range.select(); 
	}
  });
};


function JQvalidation()
{
	jQuery(document).ready(function(){
		jQuery("html,body").find('[valid="int"]').each(function(){
			jQuery(this).keypress(function(e){
				if ((e.which < 48 || e.which > 57) && e.which!=0 && e.which!=8){
					e.preventDefault();
				}
				//jQuery(this).val(jQuery(this).val().replace(/\D/g,""));
			});
		});
		
		jQuery("html,body").find('[valid="float"]').each(function(){
			jQuery(this).keyup(function(){
				floatVal=jQuery(this).val().replace(/[^0-9\.]/g,"").replace(/\./, "x").replace(/\./g, "").replace(/x/, ".");
				jQuery(this).val(floatVal);
			});
			
			jQuery(this).blur(function(){
				floatVal=jQuery(this).val().replace(/\.$/,"");
				jQuery(this).val(floatVal);
			});
		});
		
		$Arr1_=new Array();
		jQuery("html,body").find('[showvalue]').each(function(index){
			$Arr1_[index]=jQuery.trim(jQuery(this).attr('showvalue'));
			jQuery(this).val(jQuery.trim(jQuery(this).val())?jQuery.trim(jQuery(this).val()):jQuery.trim(jQuery(this).attr('showvalue')));
			
			jQuery(this).focus(function(){
				jQuery(this).val(jQuery(this).val()==$Arr1_[index]?'':jQuery(this).val());
			});
			
			jQuery(this).blur(function(){
				jQuery(this).val(jQuery(this).val()==""?$Arr1_[index]:jQuery(this).val());
			});
			
		});
	});
}

function maxZIndex()
{
	var z=0;
	jQuery('div,p,span,dd,a,table').each(function(){
		if(jQuery(this).css("zIndex") && !isNaN(jQuery(this).css("zIndex")))
		{
			if(jQuery(this).css("zIndex")>z)
				z=jQuery(this).css("zIndex");
		}
	});
	
	return z;
}

jQuery.fn.getval=function(){
	if(jQuery(this).val()==jQuery(this).attr('placeholder') || jQuery(this).val()==jQuery(this).attr('showvalue'))
		return '';
	else
		return jQuery(this).val();
};

function getCalendar(id,yStart,yEnd, noWeekend) {
	var yRange="1900:2020";
	if(yStart && yEnd){
		yRange=yStart+":"+yEnd;
	}
	
	id="#"+id;
	ddate=jQuery(id).val();
	if(noWeekend){
		jQuery(id).datepicker({ beforeShowDay: jQuery.datepicker.noWeekends });
	}
	
	jQuery(id).datepicker({changeYear:true});
	jQuery(id).datepicker({dateFormat:"dd M yy"});
	var dateFormat=$(this).datepicker("option","dateFormat");
	$(this).datepicker("option","dateFormat","dd M yy");
	jQuery(id).datepicker({yearRange:yRange});
	jQuery(id).datepicker();
	
	jQuery(id).val(ddate);
}


function setDateToInputs() {
	$=jQuery.noConflict();
	
	$('.hasCal').each(function() {
		ddate=$(this).val();
		yRange="1900:2020";
		yr=$(this).attr('year-range');
		if(yr){
			yRange=yr;
		}
		
		if($(this).attr('noweekend')){
			$(this).datepicker({ beforeShowDay: jQuery.datepicker.noWeekends });
		}
		
		$(this).datepicker({changeYear:true});
		$(this).datepicker({changeMonth:true});
		$(this).datepicker("option","changeMonth",true);
		$(this).datepicker({dateFormat:"dd M yy"});
		var dateFormat=$(this).datepicker("option","dateFormat");
		$(this).datepicker("option","dateFormat","dd M yy");
		//$(this).datepicker({yearRange:yRange});
		$(this).datepicker( "option", "yearRange", yRange);
		
		$(this).datepicker();
		
		$(this).val(ddate);
	});
}

function showDialog(msg, title, isModal) {
	title	=title || '';
	isModal =isModal || false;
	
	$(".uiDialog007").remove();
	obj=$('<div class="uiDialog007" title="'+title+'"></div>').appendTo('body');
	
	obj.html(msg).dialog({
		drag: function(e, ui){
			ui.position.top -= $(window).scrollTop();
		},
		modal:isModal
	});
}

function closeDialog() {
	$(".uiDialog007").dialog( "close" );
}

function JQCommon_() {
	jQuery(document).ready(function() {
		//jQuery('label').disableSelection();
		setDateToInputs();
	});
	
	JQvalidation();
}
JQCommon_();