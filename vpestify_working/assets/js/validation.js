  // When the browser is ready...
  $(function() {
  
  //jQuery.validator.addMethod("acceptNumber", function(value, element, param) {
    //return value.match(new RegExp("." + param + "$"));
  //});
  jQuery.validator.addMethod("acceptNumber", function(value, element) {
    var al =  /^([a-zA-Z0-9 ]+)*$/;
    return this.optional(element) || al.test(value);
  }, "Invalid Role Name");
  
  jQuery.validator.addMethod("alphabets", function(value, element) {
    var al =  /^[A-Za-z']+( [A-Za-z']+)*$/;
    return this.optional(element) || al.test(value);
  }, "Invalid");
  jQuery.validator.addMethod("description", function(value, element) {
    var al =  /^[a-zA-Z]+(?:\s*,\s*[a-zA-Z]+)*$/;
    return this.optional(element) || al.test(value);
  }, "Invalid description");
  jQuery.validator.addMethod("lccharReqs", function(value, element) {
    var lc =  /(?=.*[a-z])/;
    return this.optional(element) || lc.test(value);
  }, "Your password should contain at least one lowercase letter!");
  jQuery.validator.addMethod("uccharReqs", function(value, element) { 
    var uc = /(?=.*[A-Z])/;
    return this.optional(element) || uc.test(value);
  }, "Your password should contain at least one uppercase letter!");
  jQuery.validator.addMethod("nccharReqs", function(value, element) {
    var nc = /(?=.*[0-9])/;
    return this.optional(element) || nc.test(value);
  }, "Your password should contain at least one number!");
  jQuery.validator.addMethod("speccharReqs", function(value, element) {
    var sc = /(?=.*\W)/;
    return this.optional(element) || sc.test(value);
   }, "Your password should contain at least one special character!");
   jQuery.validator.addMethod("imageformat", function(value, element) {
    var sc = /\.(jpe?g|gif|png)$/i;
    return this.optional(element) || sc.test(value);
   }, "invalid file format");
   jQuery.validator.addMethod("isdvalidation", function(value, element) {
    var sc = /^[+1-9]\d+$/;
    return this.optional(element) || sc.test(value);
   }, "invalid format");
    // Setup form validation on the #register-form element
    $("#register-form").validate({
         rules: {
            ////// Rule Validation For User /////
            name: {
              required: true,
              alphabets: true
            },
            ///// Rule Validation For Role /////
            roleName: {
              required: true,
              acceptNumber: true
              //alphabets: true
            },
    
            emailId: {
                required: true,
                email: true
            },
            password: {
                required: true,
                lccharReqs: true,
                uccharReqs: true,
                nccharReqs: true,
                speccharReqs: true,
                minlength: 6
            },
            alternateEmailId:{
                email: true
            },
            mobileNumber:{
                number:true,
                minlength: 10
            },
            image:{
                imageformat:true
            },
            'relatedKeywords[]':{
                required: true
            },
          
			notifaction_title:{
				
				 required: true,
                alphabets: true
				},
				notifaction_message:{
				
				 required: true,
                alphabets: true
				},
				check:{
				
				 required: true,
               
				},
				categories_name:{
				
				 required: true,
                alphabets: true
				},
				payment_name:{
				
				 required: true,
                alphabets: true
				},
				
        },
        
        // Specify the validation error messages
        messages: {
            name: {
                required: "Please Enter Name",
                alphabets:"Invalid Name"
            },
			  categories_name: {
                required: "Please Enter Categories Name",
                alphabets:"Invalid Categories Name"
            },
              start_time: {
                required: "Please Enter Categories Name",
                alphabets:"Invalid Categories Name"
            },
			  payment_name: {
                required: "Please Enter Payment Name",
                alphabets:"Invalid Categories Name"
            },
            emailId: {
                required: "Please enter email-id",
                email: "Please enter a valid email address"
            },
            alternateEmailId: "Please enter a valid email address",
            roleName: {
                required: "Please enter Role Name",
               // alphabets:"Invalid Role Name"
			   roleName: "Invalid Role Name"
            },
          
            mobileNumber: {
                number: "Invalid Mobile number",
                minlength: "Invalid Mobile number"
            },
            password: {
                required: "Please provide a password",
                lccharReqs: "at least one Lower case character",
                uccharReqs: "at least one Upper case character",
                nccharReqs: "at least one Number",
                minlength: "Your password must be at least 6 characters long",
                speccharReqs: "at least one Special character!"
            },
          
            ///// Message for Keywords////
           
			 notifaction_title: {
                required: "Please enter Title",
                alphabets:"Invalid Title"
            },
			
			 notifaction_message: {
                required: "Please enter Message",
                alphabets:"Invalid Title"
            },
			 'check[]': {
                required: "Please Select User",
               // alphabets:"Invalid Title"
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
	
	//start for popup validation 
	 $("#popup-form").validate({
         rules: {
            ////// Rule Validation For User /////
            name: {
              required: true,
              alphabets: true
            },
            ///// Rule Validation For Role /////
            roleName: {
              required: true,
              acceptNumber: true
              //alphabets: true
            },
            roleId: "required",
            //roleDescription: {
               // alphabets: true
           // },
            ///// Rule for Services ////
           
            emailId: {
                required: true,
                email: true
            },
            password: {
                required: true,
                lccharReqs: true,
                uccharReqs: true,
                nccharReqs: true,
                speccharReqs: true,
                minlength: 6
            },
            alternateEmailId:{
                email: true
            },
            mobileNumber:{
                number:true,
                minlength: 10
            },
          
            ////// Rule Validation For Lookups /////
           
			notifaction_title:{
				
				 required: true,
                alphabets: true
				},
				notifaction_message:{
				
				 required: true,
                alphabets: true
				},
				check:{
				
				 required: true,
               
				}
        },
        
        // Specify the validation error messages
        messages: {
            name: {
                required: "Please Enter Name",
                alphabets:"Invalid Name"
            },
            emailId: {
                required: "Please enter email-id",
                email: "Please enter a valid email address"
            },
            alternateEmailId: "Please enter a valid email address",
            roleName: {
                required: "Please enter Role Name",
               // alphabets:"Invalid Role Name"
			   roleName: "Invalid Role Name"
            },
            //roleDescription:{
                //alphabets: "Invalid Role description"
            //},
            roleId: "Please select role of the user",
            mobileNumber: {
                number: "Invalid Mobile number",
                minlength: "Invalid Mobile number"
            },
            password: {
                required: "Please provide a password",
                lccharReqs: "at least one Lower case character",
                uccharReqs: "at least one Upper case character",
                nccharReqs: "at least one Number",
                minlength: "Your password must be at least 6 characters long",
                speccharReqs: "at least one Special character!"
            },
           
            image: {
                imageformat: "invalid file format"
            },
           
           
           
			 notifaction_title: {
                required: "Please enter Title",
                alphabets:"Invalid Title"
            },
			
			 notifaction_message: {
                required: "Please enter Message",
                alphabets:"Invalid Title"
            },
			 'check[]': {
                required: "Please Select User",
               // alphabets:"Invalid Title"
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
	//End Popup Validation 
  });